#include <amxmodx>
#include <hamsandwich>
#include <fun>

#define PLUGIN	"Advanced Render"
#define VERSION	"1.0"
#define AUTHOR	"trofian"

#define Player[%1][%2]			g_player_data[%1 - 1][%2]
#define MAX_COLORS				8

#define r 0
#define g 1
#define b 2

enum _:Player_Properties
{
	PlLastFx,
	PlLastRenderMode,
	PlLastAmount,
	PlColors		// ���������� ������-1, �.�. ��� 8 MAX_COLORS, ��� �������� 7
}

new
g_player_data[32][Player_Properties],
g_colors[33][3][MAX_COLORS]

public plugin_natives()
{
	register_native("ka_render_add", "_ka_radd", 0)
	register_native("ka_render_sub", "_ka_rsub", 0)
}

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	RegisterHam(Ham_Killed, "player", "hook_killed", 1)
	register_event("HLTV", "HLTV", "a", "1=0", "2=0")
}

public HLTV()
{
	for(new id=1; id<33; id++)
	{
		Player[id][PlColors] = -1
		Player[id][PlLastFx] = kRenderFxNone
		Player[id][PlLastRenderMode] = kRenderNormal
		Player[id][PlLastAmount] = 16
		reset_colors(id)
	}
}

public hook_killed(id)
{
	Player[id][PlColors] = -1
	Player[id][PlLastFx] = kRenderFxNone
	Player[id][PlLastRenderMode] = kRenderNormal
	Player[id][PlLastAmount] = 16
	reset_colors(id)
}

public client_putinserver(id)
	hook_killed(id)

public _ka_radd(plugin, num_params)
{
	new
	id = get_param(1),
	fx = get_param(2),
	red = get_param(3),
	green = get_param(4),
	blue = get_param(5),
	mode = get_param(6),
	amount = get_param(7)

	if(fx == Player[id][PlLastFx] && mode == Player[id][PlLastRenderMode] &&  Player[id][PlColors] < MAX_COLORS)
	{
		Player[id][PlColors]++
		
		new ret, rgb[3], c_rgb[3]
		c_rgb[r] = red
		c_rgb[g] = green
		c_rgb[b] = blue
		
		ret = add_render_colors(id, c_rgb)
		
		if(ret == -1)
		{
			reset_colors(id)
			ret = add_render_colors(id, c_rgb)
		}
		
		calculate(id, rgb)
		set_user_rendering(id, fx, red, green, blue, mode, amount)
		
		return ret
	}
	
	reset_colors(id)
	
	g_colors[id][r][0] = red
	g_colors[id][g][0] = green
	g_colors[id][b][0] = blue
	
	set_user_rendering(id, fx, red, green, blue, mode, amount)
	
	Player[id][PlColors] = 0
	Player[id][PlLastFx] = fx
	Player[id][PlLastRenderMode] = mode
	Player[id][PlLastAmount] = amount
	
	return 0
}

public _ka_rsub(plugin, num_params)
{
	new
	id = get_param(1),
	shift = get_param(2)
	
	if(Player[id][PlColors] < 0 || -1 >= shift >= MAX_COLORS)
		return -1
	
	for(new i; i<3; i++)
		g_colors[id][i][shift] = -1
	
	Player[id][PlColors]--
	
	if(Player[id][PlColors] <= -1)
	{
		Player[id][PlColors] = -1
		reset_colors(id)
		set_user_rendering(id)
		return 0
	}
	
	new rgb[3]
	calculate(id, rgb)
	set_user_rendering(id, Player[id][PlLastFx], rgb[r], rgb[g], rgb[b], Player[id][PlLastRenderMode], Player[id][PlLastAmount])
	
	return 1
}

reset_colors(id)
{
	for(new i; i<MAX_COLORS; i++)
	{
		g_colors[id][r][i] = -1
		g_colors[id][g][i] = -1
		g_colors[id][b][i] = -1
	}
}

add_render_colors(id, rgb[3])
{
	for(new i; i<MAX_COLORS; i++)
	{
		for(new j; j<3; j++)
		{
			if(g_colors[id][j][i] == -1)
			{
				g_colors[id][i][r] = rgb[r]
				g_colors[id][i][g] = rgb[g]
				g_colors[id][i][b] = rgb[b]
				return i
			}
		}
	}
	
	return -1
}

calculate(id, Rgb[3])
{
	new Float:sum[3], Float:count
	for(new i; i<MAX_COLORS; i++)
	{
		if(g_colors[id][r][i] != -1)
		{
			sum[r] += float(g_colors[id][r][i])
			sum[g] += float(g_colors[id][g][i])
			sum[b] += float(g_colors[id][b][i])
			count += 1.0
		}
	}
	
	if(count == 0)
	{
		server_print("division by zero")
		return
	}
	
	for(new i; i<3; i++)
		Rgb[i] = floatround(sum[i]/count)
}
