#include <amxmodx>
#include <engine>
#include <cstrike>
#include <next21_knife_core>
#include <WPMGPrintChatColor>

#define PLUGIN	"Next21 Bunny Item"
#define AUTHOR	"trofian"
#define VERSION	"1.0"

#define ROUNDS 6

new auto_bhop_enable[33] = ROUNDS
new g_maxpl

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	register_event("HLTV", "NewRound", "a", "1=0", "2=0")
	kc_register_shop_item("get_bonney", "Вы купили автораспрыг. Зажмите кнопку прыжка.", "Автораспрыг (5 раундов)", 23000)
	g_maxpl = get_maxplayers()
	for(new i=1; i<=g_maxpl; i++)
		auto_bhop_enable[i] = ROUNDS
}

public get_bonney(id)
{
	if(!is_user_alive(id))
		return ITEM_DEAD
	
	if(auto_bhop_enable[id] < ROUNDS)
		return ITEM_ALREADY_HAVE
	
	auto_bhop_enable[id] = 0
	
	return ITEM_CONTINUE
}

public client_PreThink(id)
{
	if (auto_bhop_enable[id] == ROUNDS)
		return PLUGIN_CONTINUE

	entity_set_float(id, EV_FL_fuser2, 0.0)

	if (entity_get_int(id, EV_INT_button) & 2)
	{
		new flags = entity_get_int(id, EV_INT_flags)
		
		if (flags & FL_WATERJUMP)
			return PLUGIN_CONTINUE
		
		if (entity_get_int(id, EV_INT_waterlevel) >= 2)
			return PLUGIN_CONTINUE
		
		if (!(flags & FL_ONGROUND))
			return PLUGIN_CONTINUE
		
		new Float:velocity[3]
		entity_get_vector(id, EV_VEC_velocity, velocity)
		velocity[2] += 250.0
		entity_set_vector(id, EV_VEC_velocity, velocity)
		
		entity_set_int(id, EV_INT_gaitsequence, 6)
	}
	
	return PLUGIN_CONTINUE
}

public client_disconnect(id)
	auto_bhop_enable[id] = ROUNDS

public NewRound()
{
	for(new i=1; i<=g_maxpl; i++)
	{
		if(auto_bhop_enable[i] < ROUNDS) 
		{
			auto_bhop_enable[i]++
			if(auto_bhop_enable[i] == ROUNDS - 1)
				PrintChatColor(i, _, "!g[%s] !yПоследний раунд с автораспрыжкой", PLUGIN)
		}
	}
}
	
