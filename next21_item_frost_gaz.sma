#include <amxmodx>
#include <fun>
#include <engine>
#include <cstrike>
#include <fakemeta>
#include <fakemeta_util>
#include <hamsandwich>
#include <next21_advanced>
#include <next21_knife_core>

#define PLUGIN "Frost & Gaz Nades"
#define VERSION "1.0"
#define AUTHOR "Psycrow"

#define FROST_RADIUS	240.0
#define FROST_R	0
#define FROST_G	206
#define FROST_B	209

#define TASK_REMOVE_CHILL	200
#define TASK_REMOVE_FREEZE	250

#define GASP_SOUND1 		"player/gasp1.wav"
#define GASP_SOUND2 		"player/gasp2.wav"

#define SMOKEGRENADE_SOUND 	"weapons/sg_explode.wav"
#define GAS_CLASSNAME 		"gas_entity"
#define GASNADE_LIFE 		25.0

#define IsEntityPlayer(%1)	(1<=%1<=g_maxplayers)

const PRIMARY_WEAPONS_BIT_SUM = (1<<CSW_SCOUT)|(1<<CSW_XM1014)|(1<<CSW_MAC10)|(1<<CSW_AUG)|(1<<CSW_UMP45)|(1<<CSW_SG550)|(1<<CSW_GALIL)|(1<<CSW_AK47)|(1<<CSW_AWP)|(1<<CSW_MP5NAVY)|(1<<CSW_M249)|(1<<CSW_M3)|(1<<CSW_M4A1)|(1<<CSW_TMP)|(1<<CSW_G3SG1)|(1<<CSW_SG552)|(1<<CSW_AK47)|(1<<CSW_P90)
new bool: g_nade_frost_hand[33]
new bool: g_nade_gaz_hand[33]
new bool: g_nade_gaz_once[33]
new bool: g_nade_gaz_changes[33]
new g_nade_frost_ammo[33]
new g_nade_gaz_ammo[33]

new MsgIndexWeaponList 
new g_maxplayers

#define g_wsmoke	"models/w_smokegrenade.mdl"

//*************���������� ��� ������*************//
#define	g_vFrost	"models/next21_knife_v2/items/frost_grenade/v_n21_frost.mdl"
#define g_pFrost	"models/next21_knife_v2/items/frost_grenade/p_n21_frost.mdl"
#define g_wFrost	"models/next21_knife_v2/items/frost_grenade/w_n21_frost.mdl"
#define g_wworldFrost	"models/next21_knife_v2/items/frost_grenade/w_n21_frost_wrld.mdl"

#define MdlFrostnova	"models/next21_knife_v2/items/frost_grenade/frostnova.mdl"

#define SoundFNova	"next21_knife_v2/items/frost_grenade/frostnova.wav"
#define SoundIHit	"next21_knife_v2/items/frost_grenade/impalehit.wav"
#define SoundILaunch	"next21_knife_v2/items/frost_grenade/impalelaunch1.wav"

new hasFrostNade[33];
new isChilled[33];
new isFrozen[33];

new novaDisplay[33];
new Float:oldSpeed[33];

new glassGibs;
new trailSpr;
new smokeSpr;
new exploSpr;
//**************************//

//*************���������� ��� ����*************//
new gmsgDamage

new Float:player_affected_time[33]
new Float:player_restored_time[33]
new Float:damage_took[33]

#define g_vGaz		"models/next21_knife_v2/items/gas_grenade/v_n21_gaz.mdl"
#define g_pGaz		"models/next21_knife_v2/items/gas_grenade/p_n21_gaz.mdl"
#define g_wGaz		"models/next21_knife_v2/items/gas_grenade/w_n21_gaz.mdl"
//**************************//

public plugin_natives()
{
	register_native("ka_gren_unfreez", "_n21_gren_unfreez", 0) // 1 - ��
	register_native("ka_gren_unchill", "_n21_gren_unchill", 0) // 1 - ��
	
	register_native("ka_frostgren_give", "_n21_frost_give", 0) // 1 - ��
	register_native("ka_gazgren_give", "_n21_gaz_give", 0) // 1 - ��
	
	register_native("ka_in_fgrenfreez", "_n21_in_fgrenfreez", 0) // 1 - ��
	register_native("ka_in_fgrenchill", "_n21_in_fgrenchill", 0) // 1 - ��
	
	register_native("ka_frostgren_inhand", "_n21_frost_inhand", 0) // 1 - ��
	register_native("ka_gazgren_inhand", "_n21_gaz_inhand", 0) // 1 - ��
	register_native("ka_frostgren_ammo", "_n21_frost_ammo", 0) // 1 - ��
	register_native("ka_gazgren_ammo", "_n21_gaz_ammo", 0) // 1 - ��
}

public plugin_precache() 
{  
	precache_generic("sprites/weapon__next21_gren_gaz.txt")
	precache_generic("sprites/weapon__next21_gren_frost.txt")

	engfunc(EngFunc_PrecacheSound, GASP_SOUND1)
	engfunc(EngFunc_PrecacheSound, GASP_SOUND2)
	
	precache_model(g_vGaz)
	precache_model(g_pGaz)
	precache_model(g_wGaz)
	precache_model(g_wsmoke)
	
	precache_sound("weapons/n21_gaz_pinpull.wav")
	
	precache_model(MdlFrostnova);
	glassGibs = precache_model("models/glassgibs.mdl");

	precache_sound(SoundFNova); // grenade explodes
	precache_sound(SoundIHit); // player is frozen
	precache_sound(SoundILaunch); // frozen wears off
	precache_sound("player/pl_duct2.wav"); // player is chilled

	trailSpr = precache_model("sprites/laserbeam.spr");
	smokeSpr = precache_model("sprites/steam1.spr");
	exploSpr = precache_model("sprites/shockwave.spr");
	
	precache_model(g_vFrost)
	precache_model(g_pFrost)
	precache_model(g_wFrost)
	precache_model(g_wworldFrost)
	precache_sound("weapons/holywater_pinpul.wav")
	precache_sound("weapons/holywater_deploy.wav")
	
	g_maxplayers = get_maxplayers()
}  

public plugin_init()  
{  
	register_plugin(PLUGIN, VERSION, AUTHOR)
	
	kc_register_shop_item("get_frost", "Вы купили замораживающую гранату", "Замораживающая граната", 4500)
	kc_register_shop_item("get_gaz", "Вы купили газовую гранату", "Газовая граната", 4500) //4500
	
	RegisterHam( Ham_Item_AddToPlayer, "weapon_xm1014", "OnAddToPlayerxm1014", .Post = true ) 
	RegisterHam( Ham_Item_AddToPlayer, "weapon_smokegrenade", "OnAddToPlayersmoke", .Post = true )
	RegisterHam( Ham_Item_ItemSlot, "weapon_xm1014", "OnItemSlotxm1014" ) 
	RegisterHam( Ham_Item_ItemSlot, "weapon_smokegrenade", "OnItemSlotsmoke" ) 
	RegisterHam(Ham_Touch, "weapon_smokegrenade", "fwdTouchSmoke")
	register_forward(FM_SetModel, "CEntity__SetModel");
	register_clcmd( "weapon__next21_gren_gaz", "ClientCommand_SelectGaz" ) 
	register_clcmd( "weapon__next21_gren_frost", "ClientCommand_SelectFrost" ) 
	MsgIndexWeaponList = get_user_msgid( "WeaponList" ) 
	
	register_event("HLTV", "eNewRound", "a", "1=0", "2=0")

	register_forward(FM_EmitSound,"fwdEmitSound")
	register_forward(FM_Touch,"fwdTouch")
	register_forward(FM_Think,"fwdThink")
	register_forward(FM_PlayerPreThink, "fwdPlayerPreThink")
	register_forward(FM_SetModel, "fw_SetModel")
	
	register_event("CurWeapon", "CurWeapon", "be","1=1", "2=9")
	register_event("CurWeapon", "CurWeaponBUGFIX", "be","1=1")
	
	gmsgDamage = get_user_msgid("Damage")
	
	register_cvar("fn_los","0");

	register_cvar("fn_maxdamage","20.0");
	register_cvar("fn_mindamage","1.0");

	register_cvar("fn_chill_maxchance","100");
	register_cvar("fn_chill_minchance","100");
	register_cvar("fn_chill_duration","8");
	register_cvar("fn_chill_speed","60");

	register_cvar("fn_freeze_maxchance","100");
	register_cvar("fn_freeze_minchance","40");
	register_cvar("fn_freeze_duration","4");

	register_event("DeathMsg","event_deathmsg","a");
	register_think("grenade","think_grenade");

	register_logevent("event_roundend",2,"0=World triggered","1=Round_End");
	
	set_frost_wmodel()
}  

public CEntity__SetModel(entity, const model[])
{
	if(!equal(model, "models/w_xm1014.mdl"))
		return
    
	new szClassname[10];
    
	entity_get_string(entity, EV_SZ_classname, szClassname, 9);
    
	if (!equal(szClassname, "weaponbox")) return;
    
	entity_set_float(entity, EV_FL_nextthink, get_gametime());
} 

public client_connect(id) {
	player_affected_time[id] = get_gametime()
	player_restored_time[id] = get_gametime()
	damage_took[id] = 0.0
	isFrozen[id] = false
	isChilled[id] = false
	g_nade_gaz_once[id] = false
	g_nade_frost_ammo[id] = 0
	g_nade_gaz_ammo[id] = 0
	g_nade_frost_hand[id] = false
	g_nade_gaz_hand[id] = false
	g_nade_gaz_changes[id] = false
}

public client_disconnect(id)
{
	isFrozen[id] = 0
}

public kill_for_kill(id)
	user_kill(id)

public ClientCommand_SelectGaz( const client )  
{  	
	g_nade_gaz_hand[client] = true
	g_nade_frost_hand[client] = false
	g_nade_gaz_changes[client] = true
	
	if(!user_has_weapon(client, CSW_SMOKEGRENADE))
		fm_give_item(client, "weapon_smokegrenade")
		
	new countSmoke = g_nade_gaz_ammo[client]
	cs_set_user_bpammo(client, CSW_SMOKEGRENADE, countSmoke)
	
	if(get_user_weapon(client) != CSW_SMOKEGRENADE)
	{
		engclient_cmd(client,"weapon_smokegrenade")
	}
	else
	{
		
		set_pev(client, pev_viewmodel2, g_vGaz)
		set_pev(client, pev_weaponmodel2, g_pGaz)
		UTIL_PlayWeaponAnimation(client, 3)
	}
}  

public ClientCommand_SelectFrost( const client )  
{
	if(g_nade_frost_ammo[client] == 0)
		ClientCommand_SelectGaz(client)
	else
	{
		g_nade_frost_hand[client] = true
		g_nade_gaz_hand[client] = false
		g_nade_gaz_changes[client] = false
	
		new countSmoke = g_nade_frost_ammo[client]
		cs_set_user_bpammo(client, CSW_SMOKEGRENADE, countSmoke)
	
		if(get_user_weapon(client) != CSW_SMOKEGRENADE)
			engclient_cmd(client,"weapon_smokegrenade")
		else
		{
			set_pev(client, pev_viewmodel2, g_vFrost)
			set_pev(client, pev_weaponmodel2, g_pFrost)
			UTIL_PlayWeaponAnimation(client, 3)
		}
	}
} 

public CurWeapon(id)
{	
	if(g_nade_gaz_changes[id])
		ClientCommand_SelectGaz(id)
	else
		ClientCommand_SelectFrost(id)
}
public CurWeaponBUGFIX(id)
{
	if(get_user_weapon(id) != CSW_SMOKEGRENADE)
	{
		g_nade_frost_hand[id] = false
		g_nade_gaz_hand[id] = false
	}
	if(get_user_weapon(id) == CSW_XM1014)
		ClientCommand_SelectGaz(id)
}

public OnAddToPlayerxm1014( const item, const player )  
{  
	if( pev_valid( item ) && is_user_alive( player ) ) // just for safety.  
	{ 
		message_begin( MSG_ONE, MsgIndexWeaponList, .player = player ) 
		{  

			write_string( "weapon__next21_gren_gaz" );  // WeaponName  

			write_byte( -1 );                   // PrimaryAmmoID  

			write_byte( -1 );                   // PrimaryAmmoMaxAmount  

			write_byte( -1 );                   // SecondaryAmmoID  

			write_byte( -1 );                   // SecondaryAmmoMaxAmount  

			write_byte( 3 );                    // SlotID (0...N)  

			write_byte( 4 );                    // NumberInSlot (1...N)  

			write_byte( CSW_XM1014 );            // WeaponID  

			write_byte(  0 );                    // Flags  
		}  
		message_end()
	}  
}  

public OnAddToPlayersmoke( const item, const player )  
{	
	if(g_nade_gaz_once[player])
	{
	if( pev_valid( item ) && is_user_alive( player ) ) // just for safety.  
		{  
		message_begin( MSG_ONE, MsgIndexWeaponList, .player = player ) 
		{  

			write_string( "weapon__next21_gren_gaz" );  // WeaponName  

			write_byte( 13 );                   // PrimaryAmmoID  

			write_byte( 1 );                   // PrimaryAmmoMaxAmount  

			write_byte( -1 );                   // SecondaryAmmoID  

			write_byte( -1 );                   // SecondaryAmmoMaxAmount  
			
			write_byte( 3 );                    // SlotID (0...N) 

			write_byte( 3 );                    // NumberInSlot (1...N)  

			write_byte( CSW_SMOKEGRENADE );            // WeaponID  

			write_byte(  24 );                    // Flags  
		}  
		message_end()
	}  
	}
	else
	{
	if( pev_valid( item ) && is_user_alive( player ) ) // just for safety.  
		{  
		message_begin( MSG_ONE, MsgIndexWeaponList, .player = player ) 
		{  

			write_string( "weapon__next21_gren_frost" );  // WeaponName  

			write_byte( 13 );                   // PrimaryAmmoID  

			write_byte( 1 );                   // PrimaryAmmoMaxAmount  

			write_byte( -1 );                   // SecondaryAmmoID  

			write_byte( -1 );                   // SecondaryAmmoMaxAmount  
			
			write_byte( 3 );                    // SlotID (0...N) 

			write_byte( 3 );                    // NumberInSlot (1...N)  

			write_byte( CSW_SMOKEGRENADE );            // WeaponID  

			write_byte(  24 );                    // Flags  
		}  
		message_end()
	}  
	}
}

public OnItemSlotxm1014( const item )  
{  
	SetHamReturnInteger( 5 )
	return HAM_SUPERCEDE
} 

public OnItemSlotsmoke( const item )  
{  
	SetHamReturnInteger( 5 )
	return HAM_SUPERCEDE
} 

public fw_SetModel(ent, model[])
{
	if(!is_valid_ent(ent))
		return FMRES_IGNORED;

	// not a smoke grenade
	if(!equali(model,g_wsmoke))
		return FMRES_IGNORED;
		
	new owner = entity_get_edict(ent,EV_ENT_owner);
	
	if(g_nade_gaz_hand[owner])
	{
		g_nade_gaz_ammo[owner] --
		engfunc(EngFunc_SetModel, ent, g_wGaz)
		if(!g_nade_gaz_ammo[owner])
		{
			if(!g_nade_gaz_once[owner])
			{
				ham_strip_weapon(owner,"weapon_xm1014")
				set_task(0.1,"zero_ammo_bugfix",owner)	
			}
			else
				g_nade_gaz_once[owner] = false
		}
		return FMRES_SUPERCEDE
	}
	
	// not yet thrown
	if(entity_get_float(ent,EV_FL_gravity) == 0.0)
		return FMRES_IGNORED;
	
	engfunc(EngFunc_SetModel, ent, g_wFrost)
	
	g_nade_frost_ammo[owner] --
	if(g_nade_frost_ammo[owner] == 0 && g_nade_gaz_ammo[owner] != 0)
	{
		g_nade_gaz_once[owner] = true
		set_task(0.1,"zero_ammo_bugfix2",owner)	
	}

	// store team in the grenade
	entity_set_int(ent,EV_INT_team,get_user_team(owner));

	// hide icon
	if(hasFrostNade[owner])
	{
		hasFrostNade[owner] = 0;
		message_begin(MSG_ONE,get_user_msgid("StatusIcon"),{0,0,0},owner);
		write_byte(0); // status (0=hide, 1=show, 2=flash)
		write_string("dmg_cold"); // sprite name
		write_byte(FROST_R); // red
		write_byte(FROST_G); // green
		write_byte(FROST_B); // blue
		message_end();
	}

	// give it a blue glow and a blue trail
	set_rendering(ent,kRenderFxGlowShell,FROST_R,FROST_G,FROST_B);
	set_beamfollow(ent,10,10,FROST_R,FROST_G,FROST_B,100);

	// hack? flag to remember to track this grenade's think
	entity_set_int(ent,EV_INT_bInDuck,1);
	
	set_task(1.6,"grenade_explode",ent);
	
	return FMRES_SUPERCEDE
}

public zero_ammo_bugfix(owner)
{
	if(!g_nade_frost_ammo[owner])
	{
		ham_strip_weapon(owner,"weapon_smokegrenade")
		engclient_cmd(owner,"weapon_knife")
	}
	else
		ClientCommand_SelectFrost(owner) 
}

public zero_ammo_bugfix2(owner)
{
	ham_strip_weapon(owner,"weapon_xm1014")	
	ham_strip_weapon(owner,"weapon_smokegrenade")	
	give_item(owner, "weapon_smokegrenade")
}

public get_frost(id)
{
	if(!is_user_alive(id))
		return ITEM_DEAD
	
	if(g_nade_gaz_once[id])
	{
		g_nade_gaz_once[id] = false
		ham_strip_weapon(id,"weapon_smokegrenade")
		
		give_item(id, "weapon_smokegrenade")
		give_item(id, "weapon_xm1014")
	}
	
	if(user_has_weapon(id, CSW_SMOKEGRENADE))
	{
		g_nade_frost_ammo[id] += 1
		client_cmd(id, "spk items/gunpickup2.wav")
		if(g_nade_frost_hand[id])
		{
			new countSmoke = cs_get_user_bpammo(id, CSW_SMOKEGRENADE)
			cs_set_user_bpammo(id, CSW_SMOKEGRENADE, countSmoke + 1)
		}
	} 
	else
	{
		g_nade_frost_ammo[id] = 1
		give_item(id, "weapon_smokegrenade")
	}
	
	if(g_nade_frost_ammo[id] == 1)
		ClientCommand_SelectFrost(id)
	
	return ITEM_CONTINUE
}

public get_gaz(id)
{
	if(!is_user_alive(id))
		return ITEM_DEAD
	
	if(g_nade_frost_ammo[id] == 0)
	{
		g_nade_gaz_once[id] = true
		g_nade_gaz_ammo[id] += 1
		client_cmd(id, "spk items/gunpickup2.wav")
		if(user_has_weapon(id, CSW_SMOKEGRENADE))
		{
			new countSmoke = cs_get_user_bpammo(id, CSW_SMOKEGRENADE)
			cs_set_user_bpammo(id, CSW_SMOKEGRENADE, countSmoke + 1)
		}
		else 
		{ 
			give_item(id, "weapon_smokegrenade")
			g_nade_gaz_ammo[id] = 1
		}
	}
	else
	{
		if(user_has_weapon(id, CSW_XM1014))
		{
			g_nade_gaz_ammo[id] += 1
			client_cmd(id, "spk items/gunpickup2.wav")
			if(g_nade_gaz_hand[id])
			{
				new countSmoke = cs_get_user_bpammo(id, CSW_SMOKEGRENADE)
				cs_set_user_bpammo(id, CSW_SMOKEGRENADE, countSmoke + 1)
			}
		}	
		else 
		{
			g_nade_gaz_ammo[id] = 1
			give_item(id, "weapon_xm1014")
		}
	}
	
	if(g_nade_gaz_ammo[id] == 1)
		set_task(0.1,"gogazfix",id)		
	
	return ITEM_CONTINUE
}

public gogazfix(id)
{
	if(!g_nade_gaz_once[id])
		engclient_cmd(id,"weapon_knife")
	else
	{
		ClientCommand_SelectGaz(id)
		set_pev(id, pev_viewmodel2, g_vGaz)
		set_pev(id, pev_weaponmodel2, g_pGaz)
	}
}
	

public eNewRound() {
	new ent = -1
	while((ent = engfunc(EngFunc_FindEntityByString, ent, "classname", GAS_CLASSNAME)) > 0) {
		engfunc(EngFunc_RemoveEntity, ent)
	}
	new players[32], inum, player
	get_players(players, inum)
	for(new i; i<inum; i++) {
		player = players[i]
		player_affected_time[player] = get_gametime()
		player_restored_time[player] = get_gametime()
		damage_took[player] = 0.0
	}
}

public fwdPlayerPreThink(id) {	
	if( !is_user_alive(id) || !is_user_connected(id) )
		return FMRES_IGNORED
	
	// if they are frozen, make sure they don't move at all
	if(isFrozen[id])
	{
		// stop motion
		entity_set_vector(id,EV_VEC_velocity,Float:{0.0,0.0,0.0});

		new button = get_user_button(id), oldbuttons = entity_get_int(id,EV_INT_oldbuttons);
		new flags = entity_get_int(id,EV_INT_flags);

		// if are on the ground and about to jump, set the gravity too high to really do so
		if((button & IN_JUMP) && !(oldbuttons & IN_JUMP) && (flags & FL_ONGROUND))
			entity_set_float(id,EV_FL_gravity,999999.9); // I CAN'T STAND THE PRESSURE

		// otherwise, set the gravity so low that they don't fall
		else
			entity_set_float(id,EV_FL_gravity,0.000001); // 0.0 doesn't work
	}
	
	// check if player has HPs to be set back
	static Float:damage_to_restore
	damage_to_restore = damage_took[id]
	if( damage_to_restore < 1.0 )
		return FMRES_IGNORED

	static Float:cycle
	cycle = 1.0 //�������� �����

	// should tell if player is still in smoke
	if( get_gametime() - player_affected_time[id] < cycle + 0.1 )
		return FMRES_IGNORED
	
	if( get_gametime() - player_restored_time[id] < cycle )
		return FMRES_IGNORED
	
	static Float:userHealth
	userHealth = float( pev(id, pev_health) )
	
	static Float:gasdmg
	gasdmg = 10.0 // ���� HP
	if( damage_to_restore >= gasdmg) {
		set_pev(id, pev_health, userHealth + gasdmg)
		damage_took[id] -= gasdmg
		
	}
	else if( damage_to_restore > 0.0 ) { //shouldn't happen unless an admin change some cvars during a round
		set_pev(id, pev_health, userHealth + damage_to_restore)
		damage_took[id] -= damage_to_restore
	}
	
	player_restored_time[id] = get_gametime()

	return FMRES_IGNORED
}

public fwdEmitSound(entity,channel,const sample[],Float:volume,Float:attenuation,fFlags,pitch)
{
	if(!equali(sample, SMOKEGRENADE_SOUND))
		return FMRES_IGNORED
	
	static owner
	owner = pev(entity, pev_owner)
	
	static Float:origin[3]
	pev(entity, pev_origin, origin)
	
	static ent
	ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"))
	set_pev(ent, pev_classname, GAS_CLASSNAME)
	set_pev(ent,pev_solid,SOLID_TRIGGER)

	static Float:radius, Float:min[3], Float:max[3]
	radius = 180.0 // ������
	for(new a; a<3; a++) {
		min[a] = -radius
		max[a] = radius
	}
	engfunc(EngFunc_SetSize, ent, min, max)

	set_pev(ent, pev_owner, owner) // cvar amx_gasobeyFF
	engfunc(EngFunc_SetOrigin,ent,origin)

	set_pev(ent, pev_nextthink, get_gametime() + GASNADE_LIFE)

	return FMRES_HANDLED
}

public fwdThink(ent) {
	static classname[33]
	pev(ent, pev_classname, classname, sizeof classname - 1)

	if(!equal(classname,GAS_CLASSNAME))
		return FMRES_IGNORED

	engfunc(EngFunc_RemoveEntity, ent)
	return FMRES_IGNORED
}

public fwdTouch(ent, id) {
	// hope there's enough filters here
	if(!is_user_alive(id) || !is_user_connected(id))
		return
	if(!pev_valid(ent)) {
		return
	}
	static classname[33]
	pev(ent, pev_classname, classname, sizeof classname - 1)
	if(!equal(classname, GAS_CLASSNAME))
		return



	if( get_gametime() - player_affected_time[id] < 1.0 )
		return

	player_affected_time[id] = get_gametime()


	static owner
	owner = pev(ent, pev_owner)  //still have to know how this changes if owner disconnects, need to be 2 to test...
	if( get_user_team(id) == get_user_team(owner) )
		return

	msg_damage(id, ent)

	static number
	number = random_num(1, 2)
	switch (number)
	{
		case 1: emit_sound(id, CHAN_VOICE, GASP_SOUND1, 1.0, ATTN_NORM, 0, PITCH_NORM)
		case 2: emit_sound(id, CHAN_VOICE, GASP_SOUND2, 1.0, ATTN_NORM, 0, PITCH_NORM)
	}

	static Float:damage
	damage = 10.0 // ���� HP
	if(damage > 0.0) {
		ExtraDamage(id, owner, damage)
		damage_took[id] += damage
	}
}

stock ExtraDamage(id, attacker, Float:damage)
{
	if(is_user_alive(id)) 
	{
		static Float:userHealth
		userHealth = float( pev(id, pev_health) )

		if(userHealth - damage < 1)
		{
			ka_set_attacker(id, attacker, 1.0)
			set_task(0.1,"kill_for_kill",id)
		}
		else
		{
			set_pev(id, pev_health, userHealth - damage)
		}
	}
}

msg_damage(id, entity)
{

	static Float:origin[3]
	pev(entity, pev_origin, origin)

	message_begin(MSG_ONE, gmsgDamage, _, id)
	write_byte(30) // dmg_save
	write_byte(30) // dmg_take

	write_long(DMG_SLOWFREEZE) // visibleDamageBits  //DMG_ACID , //

	engfunc(EngFunc_WriteCoord, origin[0])  //arkshine
	engfunc(EngFunc_WriteCoord, origin[1])
	engfunc(EngFunc_WriteCoord, origin[2])

	message_end()
}

public event_deathmsg() {
	new id = read_data(2);

	if(hasFrostNade[id])
	{
		hasFrostNade[id] = 0;
		message_begin(MSG_ONE,get_user_msgid("StatusIcon"),{0,0,0},id);
		write_byte(0); // status (0=hide, 1=show, 2=flash)
		write_string("dmg_cold"); // sprite name
		write_byte(FROST_R); // red
		write_byte(FROST_G); // green
		write_byte(FROST_B); // blue
		message_end();
	}

	if(isChilled[id])
		remove_chill(TASK_REMOVE_CHILL+id);

	if(isFrozen[id])
		remove_freeze(TASK_REMOVE_FREEZE+id);
		
	g_nade_frost_ammo[id] = 0
	g_nade_gaz_ammo[id] = 0
}
 
public think_grenade(ent)
 {
	if(!is_valid_ent(ent))
		return PLUGIN_CONTINUE;

	// hack? not a smoke grenade, or at least not a popular one
	if(!entity_get_int(ent,EV_INT_bInDuck))
		return PLUGIN_CONTINUE;

	// stop it from exploding
	return PLUGIN_HANDLED;
 }
 
public event_roundend()
 {
	new i;
	for(i=1;i<=32;i++)
	{
		if(isChilled[i])
			remove_chill(TASK_REMOVE_CHILL+i);

		if(isFrozen[i])
			remove_freeze(TASK_REMOVE_FREEZE+i);
	}
 }
 
 public grenade_explode(ent)
 {
	if(!is_valid_ent(ent))
		return;

	// make the smoke
	new origin[3], Float:originF[3];
	entity_get_vector(ent,EV_VEC_origin,originF);
	FVecIVec(originF,origin);

	message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
	write_byte(5); // TE_SMOKE
	write_coord(origin[0]); // x
	write_coord(origin[1]); // y
	write_coord(origin[2]); // z
	write_short(smokeSpr); // sprite
	write_byte(random_num(35,45)); // scale
	write_byte(5); // framerate
	message_end();

	// debug
	//show_xyz(origin,floatround(FROST_RADIUS));

	// explosion
	create_blast(origin);
	emit_sound(ent,CHAN_WEAPON,SoundFNova,1.0,ATTN_NORM,0,PITCH_NORM);

	// get grenade's owner
	new owner = entity_get_edict(ent,EV_ENT_owner);

	// get grenades team
	new nadeTeam = entity_get_int(ent,EV_INT_team);

	// collisions
	new player;
	while((player = find_ent_in_sphere(player,originF,FROST_RADIUS)) != 0)
	{
		// not a player, or a dead one
		if(!is_user_alive(player))
			continue;

		// don't hit teammates if friendlyfire is off, but don't count self as teammate
		if((!get_cvar_num("mp_friendlyfire") && nadeTeam == get_user_team(player)) && owner != player)
		{
			ka_stop_fire(player)
			continue;
		}

		// if user was frozen this check
		new wasFrozen;

		// get this player's origin for calculations
		new Float:playerOrigin[3];
		entity_get_vector(player,EV_VEC_origin,playerOrigin);

		// check for line of sight
		if(get_cvar_num("fn_los"))
		{
			new Float:endPos[3];
			trace_line(ent,originF,playerOrigin,endPos);

			// no line of sight (end point not at player's origin)
			if(endPos[0] != playerOrigin[0] && endPos[1] != playerOrigin[1] && endPos[2] != playerOrigin[2])
				continue;
		}

		// calculate our odds
		new Float:chillChance = radius_calucation(playerOrigin,originF,FROST_RADIUS,get_cvar_float("fn_chill_maxchance"),get_cvar_float("fn_chill_minchance"));
		new Float:freezeChance = radius_calucation(playerOrigin,originF,FROST_RADIUS,get_cvar_float("fn_freeze_maxchance"),get_cvar_float("fn_freeze_minchance"));

		ka_stop_fire(player)
		ka_unset_ninja(player)
		ka_stop_levitation(player)
		
		if(ka_in_ninja(player))
		{
			ka_render_add(player,  kRenderFxGlowShell, 0, 0, 0, kRenderTransAlpha,30)
			ka_hide_hat(player,0)
		}
		
		//new color_handle =
		ka_render_add(player, kRenderFxGlowShell, 0, 100, 200, kRenderNormal, 17)
		
		// deal damage
		if(get_cvar_float("fn_maxdamage") > 0.0)
		{
			new Float:damage = radius_calucation(playerOrigin,originF,FROST_RADIUS,get_cvar_float("fn_maxdamage"),get_cvar_float("fn_mindamage"));

			// half damage for friendlyfire
			if(nadeTeam == get_user_team(player))
				damage *= 0.5;

			// see if this will kill player
			if(floatround(entity_get_float(player,EV_FL_health)) - damage <= 0)
			{
				ka_set_attacker(player, owner, 1.0);
				set_task(0.1,"kill_for_kill",player)
			}

			fakedamage(player,"frostgrenade",damage,3);
		}

		// check for freeze
		if(random_num(1,100) <= floatround(freezeChance) && !isFrozen[player] && !ka_in_ffreez(player))
		{
			wasFrozen = 1;
			freeze_player(player);
			isFrozen[player] = 1;

			emit_sound(player,CHAN_BODY,SoundIHit,1.0,ATTN_NORM,0,PITCH_HIGH);
			set_task(get_cvar_float("fn_freeze_duration"),"remove_freeze",TASK_REMOVE_FREEZE+player);

			// if they don't already have a frostnova
			if(!is_valid_ent(novaDisplay[player]))
			{
				// create the entity
				new nova = create_entity("info_target");

				// give it a size
				new Float:maxs[3], Float:mins[3];
				maxs = Float:{ 8.0, 8.0, 4.0 };
				mins = Float:{ -8.0, -8.0, -4.0 };
				entity_set_size(nova,mins,maxs);

				// random orientation
				new Float:angles[3];
				angles[1] = float(random_num(0,359));
				entity_set_vector(nova,EV_VEC_angles,angles);

				// put it at their feet
				new Float:playerMins[3], Float:novaOrigin[3];
				entity_get_vector(player,EV_VEC_mins,playerMins);
				entity_get_vector(player,EV_VEC_origin,novaOrigin);
				novaOrigin[2] += playerMins[2];
				entity_set_vector(nova,EV_VEC_origin,novaOrigin);

				// mess with the model
				entity_set_model(nova,MdlFrostnova);
				entity_set_float(nova,EV_FL_animtime,1.0)
				entity_set_float(nova,EV_FL_framerate,1.0)
				entity_set_int(nova,EV_INT_sequence,0);
				set_rendering(nova,kRenderFxNone,FROST_R,FROST_G,FROST_B,kRenderTransColor,100);

				// remember this
				novaDisplay[player] = nova;
			}
		}

		// check for chill
		if(random_num(1,100) <= floatround(chillChance) && !isChilled[player] && !ka_in_fchill(player))
		{
			chill_player(player);
			isChilled[player] = 1;

			// don't play sound if player just got frozen,
			// reason being it will be overriden and I like the other sound better
			if(!wasFrozen)
				emit_sound(player,CHAN_BODY,"player/pl_duct2.wav",1.0,ATTN_NORM,0,PITCH_LOW);

			set_task(get_cvar_float("fn_chill_duration"),"remove_chill",TASK_REMOVE_CHILL+player);
		}
	}

	// get rid of the old grenade
	remove_entity(ent);
 }

 // apply the effects of being chilled
 public chill_player(id)
 {
	// don't mess with their speed if they are frozen
	if(isFrozen[id])
		set_user_maxspeed(id,1.0); // 0.0 doesn't work
	else
	{
		new speed = floatround(get_user_maxspeed(id) * (get_cvar_float("fn_chill_speed") / 100.0));
		set_user_maxspeed(id,float(speed));
	}

	if(!ka_is_flashed(id)) {
		message_begin(MSG_ONE,get_user_msgid("ScreenFade"),{0,0,0},id);
		write_short(~0); // duration
		write_short(~0); // hold time
		write_short(0x0004); // flags: FFADE_STAYOUT, ignores the duration, stays faded out until new ScreenFade message received
		write_byte(FROST_R); // red
		write_byte(FROST_G); // green
		write_byte(FROST_B); // blue
		write_byte(100); // alpha
		message_end();
	}

	// make them glow and have a trail
	set_user_rendering(id,kRenderFxGlowShell,FROST_R,FROST_G,FROST_B,kRenderNormal,1);

	// bug fix
	if(!isFrozen[id])
		set_beamfollow(id,30,8,FROST_R,FROST_G,FROST_B,100);
 }
 
 // apply the effects of being frozen
 public freeze_player(id)
 {
	new Float:speed = get_user_maxspeed(id);

	// remember their old speed for when they get unfrozen,
	// but don't accidentally save their frozen speed
	if(speed > 1.0 && speed != oldSpeed[id])
	{
		// save their unchilled speed
		if(isChilled[id])
		{
			new speed = floatround(get_user_maxspeed(id) / (get_cvar_float("fn_chill_speed") / 100.0));
			oldSpeed[id] = float(speed);
		}
		else
			oldSpeed[id] = speed;
	}

	// stop them from moving
	set_user_maxspeed(id,1.0); // 0.0 doesn't work
	entity_set_vector(id,EV_VEC_velocity,Float:{0.0,0.0,0.0});
	entity_set_float(id,EV_FL_gravity,0.000001); // 0.0 doesn't work
 }

 // a player's chill runs out
 public remove_chill(taskid)
 {
	remove_task(taskid);
	new id = taskid - TASK_REMOVE_CHILL;

	// no longer chilled
	if(!isChilled[id])
		return;
		
	ka_render_add(id)

	isChilled[id] = 0;

	// only apply effects to this player if they are still connected
	if(is_user_connected(id))
	{
		// clear screen fade
		message_begin(MSG_ONE,get_user_msgid("ScreenFade"),{0,0,0},id);
		write_short(0); // duration
		write_short(0); // hold time
		write_short(0); // flags
		write_byte(0); // red
		write_byte(0); // green
		write_byte(0); // blue
		write_byte(0); // alpha
		message_end();

		// restore speed and remove glow
		new speed = floatround(get_user_maxspeed(id) / (get_cvar_float("fn_chill_speed") / 100.0));
		set_user_maxspeed(id,float(speed));
		set_user_rendering(id);
		kc_reset_speed(id)
		kc_reset_gravity(id)

		// kill their trail
		message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
		write_byte(99); // TE_KILLBEAM
		write_short(id);
		message_end();
	}
 }

 
 // a player's freeze runs out
 public remove_freeze(taskid)
 {
	remove_task(taskid);
	new id = taskid - TASK_REMOVE_FREEZE;

	// no longer frozen
	if(!isFrozen[id])
		return;

	// if nothing happened to the model
	if(is_valid_ent(novaDisplay[id]))
	{
		// get origin of their frost nova
		new origin[3], Float:originF[3];
		entity_get_vector(novaDisplay[id],EV_VEC_origin,originF);
		FVecIVec(originF,origin);

		// add some tracers
		message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
		write_byte(14); // TE_IMPLOSION
		write_coord(origin[0]); // x
		write_coord(origin[1]); // y
		write_coord(origin[2] + 8); // z
		write_byte(64); // radius
		write_byte(10); // count
		write_byte(3); // duration
		message_end();

		// add some sparks
		message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
		write_byte(9); // TE_SPARKS
		write_coord(origin[0]); // x
		write_coord(origin[1]); // y
		write_coord(origin[2]); // z
		message_end();

		// add the shatter
		message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
		write_byte(108); // TE_BREAKMODEL
		write_coord(origin[0]); // x
		write_coord(origin[1]); // y
		write_coord(origin[2] + 24); // z
		write_coord(16); // size x
		write_coord(16); // size y
		write_coord(16); // size z
		write_coord(random_num(-50,50)); // velocity x
		write_coord(random_num(-50,50)); // velocity y
		write_coord(25); // velocity z
		write_byte(10); // random velocity
		write_short(glassGibs); // model
		write_byte(10); // count
		write_byte(25); // life
		write_byte(0x01); // flags: BREAK_GLASS
		message_end();

		// play a sound and remove the model
		emit_sound(novaDisplay[id],CHAN_BODY,SoundILaunch,1.0,ATTN_NORM,0,PITCH_LOW);
		remove_entity(novaDisplay[id]);
	}

	isFrozen[id] = 0;
	novaDisplay[id] = 0;

	// only apply effects to this player if they are still connected
	if(is_user_connected(id))
	{
		// restore gravity
		entity_set_float(id,EV_FL_gravity,1.0);

		// if they are still chilled, set the speed rightly so. otherwise, restore it to complete regular.
		if(isChilled[id])
		{
			set_beamfollow(id,30,8,FROST_R,FROST_G,FROST_B,100); // bug fix

			new speed = floatround(oldSpeed[id] * (get_cvar_float("fn_chill_speed") / 100.0));
			set_user_maxspeed(id,float(speed));
		}
		else
			set_user_maxspeed(id,oldSpeed[id]);
	}

	oldSpeed[id] = 0.0;
 }

 /*----------------
  UTILITY FUNCTIONS
 -----------------*/

 // my own radius calculations...
 //
 // 1. figure out how far a player is from a center point
 // 2. figure the percentage this distance is of the overall radius
 // 3. find a value between maxVal and minVal based on this percentage
 //
 // example: origin1 is 96 units away from origin2, and radius is 240.
 // this player is then 60% towards the center from the edge of the sphere.
 // let us say maxVal is 100.0 and minVal is 25.0. 60% progression from minimum
 // to maximum becomes 70.0. tada!
 public Float:radius_calucation(Float:origin1[3],Float:origin2[3],Float:radius,Float:maxVal,Float:minVal)
 {
	if(maxVal <= 0.0)
		return 0.0;

	if(minVal >= maxVal)
		return minVal;

	new Float:percent;

	// figure out how far away the points are
	new Float:distance = vector_distance(origin1,origin2);

	// if we are close enough, assume we are at the center
	if(distance < 40.0)
		return maxVal;

	// otherwise, calculate the distance range
	else
		percent = 1.0 - (distance / radius);

	// we have the technology...
	return minVal + (percent * (maxVal - minVal));
 }

 // displays x y z axis
 public show_xyz(origin[3],radius)
 {
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
	write_byte(0); // TE_BEAMPOINTS
	write_coord(origin[0]); // start x
	write_coord(origin[1]); // starty
	write_coord(origin[2] + 1); // start z
	write_coord(origin[0] + radius); // end x
	write_coord(origin[1]); // end y
	write_coord(origin[2] + 1); // end z
	write_short(trailSpr); // sprite
	write_byte(0); // starting frame
	write_byte(0); // framerate
	write_byte(100); // life
	write_byte(8); // line width
	write_byte(0); // noise
	write_byte(255); // r
	write_byte(0); // g
	write_byte(0); // b
	write_byte(200); // brightness
	write_byte(0); // scroll speed
	message_end();

	message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
	write_byte(0); // TE_BEAMPOINTS
	write_coord(origin[0]); // start x
	write_coord(origin[1]); // starty
	write_coord(origin[2] + 1); // start z
	write_coord(origin[0]); // end x
	write_coord(origin[1] + radius); // end y
	write_coord(origin[2] + 1); // end z
	write_short(trailSpr); // sprite
	write_byte(0); // starting frame
	write_byte(0); // framerate
	write_byte(100); // life
	write_byte(8); // line width
	write_byte(0); // noise
	write_byte(0); // r
	write_byte(255); // g
	write_byte(0); // b
	write_byte(200); // brightness
	write_byte(0); // scroll speed
	message_end();

	message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
	write_byte(0); // TE_BEAMPOINTS
	write_coord(origin[0]); // start x
	write_coord(origin[1]); // starty
	write_coord(origin[2]); // start z
	write_coord(origin[0]); // end x
	write_coord(origin[1]); // end y
	write_coord(origin[2] + radius); // end z
	write_short(trailSpr); // sprite
	write_byte(0); // starting frame
	write_byte(0); // framerate
	write_byte(100); // life
	write_byte(8); // line width
	write_byte(0); // noise
	write_byte(0); // r
	write_byte(0); // g
	write_byte(255); // b
	write_byte(200); // brightness
	write_byte(0); // scroll speed
	message_end();
 }

 // give an entity a trail
 public set_beamfollow(ent,life,width,r,g,b,brightness)
 {
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
	write_byte(22); // TE_BEAMFOLLOW
	write_short(ent); // ball
	write_short(trailSpr); // sprite
	write_byte(life); // life
	write_byte(width); // width
	write_byte(r); // r
	write_byte(g); // g
	write_byte(b); // b
	write_byte(brightness); // brightness
	message_end();
 }

 // blue blast
 public create_blast(origin[3])
 {
	// smallest ring
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
	write_byte(21); // TE_BEAMCYLINDER
	write_coord(origin[0]); // start X
	write_coord(origin[1]); // start Y
	write_coord(origin[2]); // start Z
	write_coord(origin[0]); // something X
	write_coord(origin[1]); // something Y
	write_coord(origin[2] + 385); // something Z
	write_short(exploSpr); // sprite
	write_byte(0); // startframe
	write_byte(0); // framerate
	write_byte(4); // life
	write_byte(60); // width
	write_byte(0); // noise
	write_byte(FROST_R); // red
	write_byte(FROST_G); // green
	write_byte(FROST_B); // blue
	write_byte(100); // brightness
	write_byte(0); // speed
	message_end();

	// medium ring
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
	write_byte(21); // TE_BEAMCYLINDER
	write_coord(origin[0]); // start X
	write_coord(origin[1]); // start Y
	write_coord(origin[2]); // start Z
	write_coord(origin[0]); // something X
	write_coord(origin[1]); // something Y
	write_coord(origin[2] + 470); // something Z
	write_short(exploSpr); // sprite
	write_byte(0); // startframe
	write_byte(0); // framerate
	write_byte(4); // life
	write_byte(60); // width
	write_byte(0); // noise
	write_byte(FROST_R); // red
	write_byte(FROST_G); // green
	write_byte(FROST_B); // blue
	write_byte(100); // brightness
	write_byte(0); // speed
	message_end();

	// largest ring
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
	write_byte(21); // TE_BEAMCYLINDER
	write_coord(origin[0]); // start X
	write_coord(origin[1]); // start Y
	write_coord(origin[2]); // start Z
	write_coord(origin[0]); // something X
	write_coord(origin[1]); // something Y
	write_coord(origin[2] + 555); // something Z
	write_short(exploSpr); // sprite
	write_byte(0); // startframe
	write_byte(0); // framerate
	write_byte(4); // life
	write_byte(60); // width
	write_byte(0); // noise
	write_byte(FROST_R); // red
	write_byte(FROST_G); // green
	write_byte(FROST_B); // blue
	write_byte(100); // brightness
	write_byte(0); // speed
	message_end();

	// light effect
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
	write_byte(27); // TE_DLIGHT
	write_coord(origin[0]); // x
	write_coord(origin[1]); // y
	write_coord(origin[2]); // z
	write_byte(floatround(FROST_RADIUS/5.0)); // radius
	write_byte(FROST_R); // r
	write_byte(FROST_G); // g
	write_byte(FROST_B); // b
	write_byte(8); // life
	write_byte(60); // decay rate
	message_end();
}

stock ham_strip_weapon(id,weapon[])
{
    if(!equal(weapon,"weapon_",7)) return 0
 
    new wId = get_weaponid(weapon)
    if(!wId) return 0
 
    new wEnt
    while((wEnt = engfunc(EngFunc_FindEntityByString,wEnt,"classname",weapon)) && pev(wEnt,pev_owner) != id) {}
    if(!wEnt) return 0
 
    if(get_user_weapon(id) == wId) ExecuteHamB(Ham_Weapon_RetireWeapon,wEnt)
 
    if(!ExecuteHamB(Ham_RemovePlayerItem,id,wEnt)) return 0
    ExecuteHamB(Ham_Item_Kill,wEnt);
 
    set_pev(id,pev_weapons,pev(id,pev_weapons) & ~(1<<wId))
     
    return 1
}

stock UTIL_PlayWeaponAnimation(const Player, const Sequence)
{
   set_pev(Player, pev_weaponanim, Sequence)
   
   message_begin(MSG_ONE_UNRELIABLE, SVC_WEAPONANIM, .player = Player)
   write_byte(Sequence)
   write_byte(pev(Player, pev_body))
   message_end()
}

public fwdTouchSmoke(ent, id) 
{
	if(g_nade_frost_ammo[id] == 0 && g_nade_gaz_ammo[id] == 0)
	{
		get_frost(id)
		get_gaz(id)
	}
}

set_frost_wmodel()
{
	new ent = get_maxplayers();
	while ((ent = engfunc(EngFunc_FindEntityByString, ent, "model", g_wsmoke)))
		engfunc(EngFunc_SetModel, ent, g_wworldFrost);
}
 
public _n21_in_fgrenfreez(plugin, num_params)
{
	if(!IsEntityPlayer(get_param(1)))
		return false
	
	return isFrozen[get_param(1)]
}

public _n21_in_fgrenchill(plugin, num_params)
{
	if(!IsEntityPlayer(get_param(1)))
		return false
	
	return isChilled[get_param(1)]
}
 
public _n21_gren_unfreez(plugin, num_params)
{
	new id = get_param(1)
	remove_freeze(TASK_REMOVE_FREEZE+id)
}
 
public _n21_gren_unchill(plugin, num_params)
{
	new id = get_param(1)
	if(isChilled[id])
		remove_chill(TASK_REMOVE_CHILL+id)	
}

public _n21_frost_give(plugin, num_params)
{
	new id = get_param(1)
	
	if(IsEntityPlayer(id))
		get_frost(id)
}
 
public _n21_gaz_give(plugin, num_params)
{
	new id = get_param(1)
	
	if(IsEntityPlayer(id))
		get_gaz(id)	
}

public _n21_frost_inhand(plugin, num_params)
{
	new id = get_param(1)
	
	if(!IsEntityPlayer(id)) return false
		
	if(g_nade_frost_hand[id])
		return true
	
	return false
}

public _n21_gaz_inhand(plugin, num_params)
{
	new id = get_param(1)
	
	if(!IsEntityPlayer(id)) return false
		
	if(g_nade_gaz_hand[id])
		return true
	
	return false	
}

public _n21_frost_ammo(plugin, num_params)
{
	new id = get_param(1)
	
	if(!IsEntityPlayer(id)) return PLUGIN_CONTINUE
		
	new ammo = g_nade_frost_ammo[id]
	
	return ammo

}

public _n21_gaz_ammo(plugin, num_params)
{
	new id = get_param(1)
	
	if(!IsEntityPlayer(id)) return PLUGIN_CONTINUE
		
	new ammo = g_nade_gaz_ammo[id]
	
	return ammo	
}
