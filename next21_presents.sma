#include <amxmodx>
#include <hamsandwich>
#include <cstrike>
#include <fakemeta_util>
#include <next21_advanced>
#include <WPMGPrintChatColor>

#define PLUGIN	"Presents"
#define AUTHOR	"trofian"
#define VERSION	"1.0"

#define ADMIN_FLAG ADMIN_KICK

#define MAX_MONEY 55000

#if cellbits == 32
	#define OFFSET_CSMONEY 115
#else
	#define OFFSET_CSMONEY 140
#endif

static g_PresentMdl[][] = {
	"models/next21_knife_v2/presents/w_present1.mdl",
	"models/next21_knife_v2/presents/w_present2.mdl",
	"models/next21_knife_v2/presents/w_present3.mdl",
	"models/next21_knife_v2/presents/w_present4.mdl"
}

static Float:fMax[3] = { 15.0, 15.0, 30.0 }
static Float:fMins[3] = { -15.0, -15.0, -4.0 }

new g_maxplayers

new g_msgMoney

public plugin_precache()
{
	for(new i=0; i<sizeof(g_PresentMdl); i++)
		precache_model(g_PresentMdl[i])
}

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	
	RegisterHam(Ham_Killed, "player", "present_drop")
	RegisterHam(Ham_Touch, "player", "present_give", 1)
	g_maxplayers = get_maxplayers()
	g_msgMoney = get_user_msgid("Money")
}

public present_drop(victim, attacker, corpse)
{
	if(!IsEntityPlayer(attacker))
		return
	
	if(1 != random_num(0,10) && !ka_is_in_candy(attacker))
		return
		
	new Float:fOriginVictim[3]
	pev(victim, pev_origin, fOriginVictim)
	
	new Float:fFloats[4]
	fFloats[0] = random_float(-25.0, -15.0)
	fFloats[1] = random_float(25.0, 15.0)
	fFloats[2] = random_float(-25.0, -15.0)
	fFloats[3] = random_float(25.0, 15.0)
	
	fOriginVictim[0] += fFloats[random_num(0,1)]
	fOriginVictim[1] += fFloats[random_num(2,3)]
	
	new ePresent = create_present(fOriginVictim)
	set_pev(ePresent, pev_velocity, {0.0, 0.0, 60.0})
}

public present_give(toucher, touched)
{
	if(!IsEntityPlayer(toucher))
		return
	
	new classname[33]
	pev(touched, pev_classname, classname, charsmax(classname))
	
	if(!equal(classname, "next21_present"))
		return
	
	present_remove(touched)
	
	new id = toucher
	
	new name[64]
	get_user_name(id, name, charsmax(name))
	
	switch(random_num(0,9))
	{
		case 0:
		{
			PrintChatColor(0, PRINT_COLOR_PLAYERTEAM + id, "!g[%s] !yИгрок !t%s !yполучает комплект гранат! (4 Ipad)", PLUGIN, name)
			if(user_has_weapon(id, CSW_HEGRENADE))
			{
				new countHe = cs_get_user_bpammo(id, CSW_HEGRENADE)
				cs_set_user_bpammo(id, CSW_HEGRENADE, countHe + 4)
			} 
			else 
			{
				fm_give_item(id, "weapon_hegrenade")
				cs_set_user_bpammo(id, CSW_HEGRENADE, 4)
			}
		}
		case 1:
		{
			PrintChatColor(0, PRINT_COLOR_PLAYERTEAM + id, "!g[%s] !yИгрок !t%s !yполучает +50 hp!", PLUGIN, name)
			fm_set_user_health(id, floatround(pev(id, pev_health)+50.0))
		}
		case 2:
		{
			PrintChatColor(0, PRINT_COLOR_PLAYERTEAM + id, "!g[%s] !yИгрок !t%s !yполучает $7000 !", PLUGIN, name)
			
			new curr_money = get_pdata_int(id, OFFSET_CSMONEY)
			
			if(curr_money+7000 > MAX_MONEY)
				curr_money = MAX_MONEY-7000
			
			set_pdata_int(id, OFFSET_CSMONEY, curr_money+7000)
			
			message_begin(MSG_ONE, g_msgMoney, _, id)
			write_long(curr_money+7000)
			write_byte(1)
			message_end()
		}
		case 3:
		{
			PrintChatColor(0, PRINT_COLOR_PLAYERTEAM + id, "!g[%s] !yИгрок !t%s !yполучает комплект гранат! (3 Замораживающих)", PLUGIN, name)
			ka_frostgren_give(id)
			ka_frostgren_give(id)
			ka_frostgren_give(id)
		}
		
		case 4:
		{
			PrintChatColor(0, PRINT_COLOR_PLAYERTEAM + id, "!g[%s] !yИгрок !t%s !yполучает комплект гранат! (2 газовых)", PLUGIN, name)
			ka_gazgren_give(id)
			ka_gazgren_give(id)
		}
		case 5:
		{
			PrintChatColor(0, PRINT_COLOR_PLAYERTEAM + id, "!g[%s] !yИгрок !t%s !yполучает комплект гранат! (1 Слеповую)", PLUGIN, name)			
			if(user_has_weapon(id, CSW_FLASHBANG))
			{
				new countFlash = cs_get_user_bpammo(id, CSW_FLASHBANG)
				cs_set_user_bpammo(id, CSW_FLASHBANG, countFlash + 1)
			} 
			else 
			{
			fm_give_item(id, "weapon_flashbang")
			cs_set_user_bpammo(id, CSW_FLASHBANG, 1)
			}
		}
		case 6:
		{
			new Players[32]
			new Count
			if(cs_get_user_team(id) == CS_TEAM_CT)
				get_players(Players,Count,"beh","CT")
			if(cs_get_user_team(id) == CS_TEAM_T)
				get_players(Players,Count,"beh","T")
	
			if(Count != 0) {		
				new player, i
				
				i = random_num(0,Count-1)
				player = Players[i]
				PrintChatColor(0, PRINT_COLOR_PLAYERTEAM + id, "!g[%s] !yИгрок !t%s !yвозрождает случайного игрока из своей команды", PLUGIN, name)
				ExecuteHam(Ham_CS_RoundRespawn, player)
			}
			else PrintChatColor(0, PRINT_COLOR_PLAYERTEAM + id, "!g[%s] !yИгрок !t%s !yничего не получает (trollface.jpg)", PLUGIN, name)
		}
		case 7:
		{
			ka_use_regeneration(id)
			PrintChatColor(0, PRINT_COLOR_PLAYERTEAM + id, "!g[%s] !yИгрок !t%s !yполучает регенерацию на 200hp", PLUGIN, name)
		}
		case 8:
		{
			new otvet = ka_next_result_mg()
			
			PrintChatColor(0, PRINT_COLOR_PLAYERTEAM + id, "!g[%s] !yИгрок !t%s !yполучает ответ на следующий пример", PLUGIN, name)
			PrintChatColor(id, _, "!g[%s] !tОтвет !yна следующий пример !t'%d'", PLUGIN, otvet)
		}
		case 9:
		{
			PrintChatColor(0, PRINT_COLOR_PLAYERTEAM + id, "!g[%s] !yИгрок !t%s !yничего не получает (trollface.jpg)", PLUGIN, name)
		}
	}
}

bool:IsEntityPlayer(id)
{
	if(1<=id<=g_maxplayers) return true
	return false
}

public present_remove(ent)
	if(pev_valid(ent)) engfunc(EngFunc_RemoveEntity, ent)

public create_present(Float:fOrigin[3])
{
	new entP = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"))
	set_pev(entP, pev_classname, "next21_present")
	set_pev(entP, pev_origin, fOrigin)
	engfunc(EngFunc_SetModel, entP, g_PresentMdl[random_num(0, charsmax(g_PresentMdl))])
	set_pev(entP, pev_solid, SOLID_BBOX)
	set_pev(entP, pev_movetype, MOVETYPE_PUSHSTEP)
	engfunc(EngFunc_SetSize, entP, fMins, fMax)
	set_pev(entP, pev_gravity, 1.0)
	
	return entP
}

stock print_col_chat(const id, const input[], any:...) 
{ 
	new count = 1, players[32]
	static msg[191]
	vformat(msg, 190, input, 3)
	replace_all(msg, 190, "!g", "^4") // Green Color 
	replace_all(msg, 190, "!y", "^1") // Default Color
	replace_all(msg, 190, "!t", "^3") // Team Color 
	
	if (id)
		players[0] = id
	else
		get_players(players, count, "ch")
	
	{
		for ( new i = 0; i < count; i++ )
		{
			if (is_user_connected(players[i]))
			{
				message_begin(MSG_ONE_UNRELIABLE, get_user_msgid("SayText"), _, players[i])
				write_byte(players[i])
				write_string(msg)
				message_end()
			}
		}
	}
}
