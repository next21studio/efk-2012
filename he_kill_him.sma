#include <amxmodx>
#include <cstrike>
#include <fun>

#define PLUGIN	"He kill Him"
#define AUTHOR	"trofian"
#define VERSION	"1.1"

#define is_entity_player(%1) (1<=%1<=g_maxplayers)

new UserAttacker[33], g_msgDeathMsg, g_msgScoreInfo, g_maxplayers

public plugin_natives()
	register_native("ka_set_attacker", "_ka_set_attacker", 0) // 1 - ������, 2 - ���������, 3 - ����� ��������

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	
	g_msgDeathMsg = get_user_msgid("DeathMsg")
	g_msgScoreInfo = get_user_msgid("ScoreInfo")
	g_maxplayers = get_maxplayers()
	
	register_message(g_msgDeathMsg, "DeathMsg")
}

public DeathMsg()
{
	new victim = get_msg_arg_int(2)
	new attacker = get_msg_arg_int(1)
	
	if(UserAttacker[victim] == 0)
		return PLUGIN_CONTINUE
	
	if(is_entity_player(attacker))
		return PLUGIN_CONTINUE
	
	make_deathmsg(UserAttacker[victim], victim, 0, "worldspawn")
	
	new FragCount = get_user_frags(UserAttacker[victim]) + 1
	set_user_frags(UserAttacker[victim], FragCount)
	set_user_frags_SCOREBOARD(UserAttacker[victim], FragCount)
	
	cs_set_user_money(UserAttacker[victim], cs_get_user_money(UserAttacker[victim])+300)
	
	return PLUGIN_HANDLED
}

public clear(id) UserAttacker[id] = 0

public set_user_frags_SCOREBOARD(id, frags)
{
	message_begin(MSG_ONE_UNRELIABLE, g_msgScoreInfo, _, id)
	write_byte(id)
	write_short(frags)
	write_short(get_user_deaths(id))
	write_short(0)
	write_short(get_user_team(id))
	message_end()
}

public _ka_set_attacker(plugin, num_params)
{
	UserAttacker[get_param(1)] = get_param(2)
	set_task(get_param_f(3), "clear", get_param(1))
}
