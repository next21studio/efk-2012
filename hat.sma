#include <amxmodx>
#include <amxmisc>
#include <cstrike>
#include <fakemeta_util>
#include <next21_advanced>
#include <next21_knife_core>
#include <WPMGPrintChatColor>

new g_bwEnt[33]

#define PLUG_NAME "Hats"
#define PLUG_AUTH "trofian"
#define PLUG_VERS "1.2"

#define menusize 	1124

#define HATS_PATH 	"models/next21_hats"

new HatFile[128]
new MenuPages, TotalHats
new CurrentMenu[33]

new UserHaveHatWith[33] = 'o' // s - skin, m - two models, n - normal hat, o - not connected
new UserHatModel[33][256]

#define MAX_HATS 64
new HATMDL[MAX_HATS][128]
new HATNAME[MAX_HATS][128]

new resethud_id[33]
new resethud_mn[33]
new resethud_tg[33]

public plugin_natives()
{
	register_native("ka_get_hat_model", "_n21_get_hat_model", 0) // 1 - id ������, 2 - ������, 3 - �������� ����������� NULL ���� ���� �����
	register_native("ka_set_hat_model", "_n21_set_hat_model", 0) // 1 - id ������, 2 - ������, NULL ���� ������ �����
	register_native("ka_hide_hat", "_n21_hide_hat", 0)			// 1 - id ������, 2 - 1 - hide, 0 - unhide
}

public plugin_init()
{
	register_plugin(PLUG_NAME, PLUG_VERS, PLUG_AUTH)
	kc_register_custom_menu_item("Шапки", "ShowMenu")
	register_concmd("amx_givehat", "Give_Hat", ADMIN_RCON, "<nick> <mdl #>")
	register_concmd("amx_removehats", "Remove_Hat", ADMIN_RCON, " - Removes hats from everyone.")
	register_menucmd(register_menuid("\yHat Menu: [Page"),(1<<0|1<<1|1<<2|1<<3|1<<4|1<<5|1<<6|1<<7|1<<8|1<<9),"MenuCommand")
	register_clcmd("say /hats",		"ShowMenu", -1, "Shows Knife menu")
	register_clcmd("hats",		"ShowMenu", -1, "Shows Knife menu")
	register_event("ResetHUD", "ResetHUD", "be")
}

public ResetHUD(id) set_task(2.0, "reload_hat", id)

public reload_hat(id)
{
	if(!is_user_alive(id))
		return
	
	new CsTeams:team = cs_get_user_team(id)
	
	if(resethud_id[id] != 0 && resethud_mn[id] != 0 && resethud_tg[id] != 0)
	{
		Set_Hat(resethud_id[id], resethud_mn[id], resethud_tg[id])
		
		resethud_id[id] = 0
		resethud_mn[id] = 0
		resethud_tg[id] = 0
	}
	
	if(!pev_valid(g_bwEnt[id]))
		return
	
	if(UserHaveHatWith[id] == 's')
	{
		if(team == CS_TEAM_CT)
			set_pev(g_bwEnt[id], pev_skin, 1)
		else if(team == CS_TEAM_T)
			set_pev(g_bwEnt[id], pev_skin, 0)
	}
	else if(UserHaveHatWith[id] == 'm')
	{
		new hatmodel[256]
		
		if(team == CS_TEAM_CT)
			formatex(hatmodel, charsmax(hatmodel), "%s/c_%s", HATS_PATH, UserHatModel[id])
		else if(team == CS_TEAM_T)
			formatex(hatmodel, charsmax(hatmodel), "%s/t_%s", HATS_PATH, UserHatModel[id])
		
		engfunc(EngFunc_SetModel, g_bwEnt[id], hatmodel)
	}
}

public ShowMenu(id)
{
	CurrentMenu[id] = 1
	ShowHats(id)
	return PLUGIN_HANDLED
}

public ShowHats(id)
{
	new keys = (1<<0|1<<1|1<<2|1<<3|1<<4|1<<5|1<<6|1<<7|1<<8|1<<9)
	
	new szMenuBody[menusize + 1], WpnID
	new nLen = format(szMenuBody, menusize, "\yHat Menu: [Page %i/%i]^n",CurrentMenu[id],MenuPages)
	
	// Get Hat Names And Add Them To The List
	for (new hatid=0; hatid < 8; hatid++) {
		WpnID = ((CurrentMenu[id] * 8) + hatid - 8)
		if (WpnID < TotalHats) {
			new hatText[512]
			if(WpnID > 0)
			{
				if(HATNAME[WpnID][0] == 's')
					format(hatText, charsmax(hatText), "\y%s \w[\rцвет команды\w]", HATNAME[WpnID][1])
				else if(HATNAME[WpnID][0] == 'm')
					format(hatText, charsmax(hatText), "\y%s \w[\rмодель команды\w]", HATNAME[WpnID][1])
				else
					format(hatText, charsmax(hatText), "\y%s", HATNAME[WpnID])
			}
			else
				format(hatText, charsmax(hatText), "\r%s", HATNAME[WpnID])
			nLen += format(szMenuBody[nLen], menusize-nLen, "^n\w %i. %s",hatid + 1,hatText)
		}
	}
	
	// Next Page And Previous/Close
	if (CurrentMenu[id] == MenuPages) {
		nLen += format(szMenuBody[nLen], menusize-nLen, "^n^n\d9. Вперед")
	} else {
		nLen += format(szMenuBody[nLen], menusize-nLen, "^n^n\w9. Вперед")
	}
	
	if (CurrentMenu[id] > 1) {
		nLen += format(szMenuBody[nLen], menusize-nLen, "^n\w0. Назад")
	} else {
		nLen += format(szMenuBody[nLen], menusize-nLen, "^n\w0. Выход")
	}
	show_menu(id, keys, szMenuBody, -1)
	return PLUGIN_HANDLED
}
public MenuCommand(id, key) 
{
	switch(key)
	{
		case 8:		//9 - [Next Page]
		{
			if (CurrentMenu[id] < MenuPages) CurrentMenu[id]++
			ShowHats(id)
			return PLUGIN_HANDLED
		}
		case 9:		//0 - [Close]
		{
			CurrentMenu[id]--
			if (CurrentMenu[id] > 0) ShowHats(id)
			return PLUGIN_HANDLED
		}
		default:
		{
			new HatID = ((CurrentMenu[id] * 8) + key - 8)
			if (HatID < TotalHats) {
				Set_Hat(id,HatID,id)
			}
		}
	}
	return PLUGIN_HANDLED
}

public plugin_precache()
{
	new cfgDir[32]
	get_configsdir(cfgDir,31)
	formatex(HatFile,63,"%s/HatList.ini",cfgDir)
	command_load()
	
	for (new i = 1; i < TotalHats; ++i) {
		if(HATNAME[i][0] == 'm')
		{
			new CurrFile[256]
			
			formatex(CurrFile, charsmax(CurrFile), "%s/t_%s", HATS_PATH, HATMDL[i])
			if(file_exists(CurrFile)) precache_model(CurrFile)
			
			formatex(CurrFile, charsmax(CurrFile), "%s/c_%s", HATS_PATH, HATMDL[i])
			if(file_exists(CurrFile)) precache_model(CurrFile)
		}
		else
		{
			new CurrFile[256]
			formatex(CurrFile, charsmax(CurrFile), "%s/%s", HATS_PATH, HATMDL[i])
			
			if (file_exists (CurrFile))
			{
				precache_model(CurrFile)
				server_print("[%s] Precached %s",PLUG_NAME,CurrFile)
			}
			else
				server_print("[%s] Failed to precache %s",PLUG_NAME,CurrFile)
		}
	}
}

public client_putinserver(id)
{
	new hatmdl[128]
	get_user_info(id, "next21_hat", hatmdl, charsmax(hatmdl))
	ka_set_hat_model(id, hatmdl)
}

public client_connect(id)
{
	if(g_bwEnt[id] > 0) engfunc(EngFunc_RemoveEntity,g_bwEnt[id])
	g_bwEnt[id] = 0
}

public client_disconnect(id)
{
	if(g_bwEnt[id] > 0) engfunc(EngFunc_RemoveEntity,g_bwEnt[id])
	
	g_bwEnt[id] = 0
	UserHaveHatWith[id] = 'o'
	formatex(UserHatModel[id], 255, "!NULL")
	
	resethud_id[id] = 0
	resethud_mn[id] = 0
	resethud_tg[id] = 0
}

public Give_Hat(id)
{
	new smodelnum[5], name[32]
	read_argv(1,name,31)
	read_argv(2,smodelnum,4)
	
	new player = cmd_target(id,name,2)
	if (!player) {
		PrintChatColor(id, _, "!g[%s] !yИгрок с таким именем не найден", PLUG_NAME)
		return PLUGIN_HANDLED
	}
	
	new imodelnum = (str_to_num(smodelnum))
	if (imodelnum > MAX_HATS) return PLUGIN_HANDLED
	
	Set_Hat(player,imodelnum,id)

	return PLUGIN_CONTINUE
}

public Remove_Hat(id)
{
	for (new i = 0; i < get_maxplayers(); ++i) {
		if (is_user_connected(i) && g_bwEnt[i] > 0) {
			engfunc(EngFunc_RemoveEntity,g_bwEnt[i])
			g_bwEnt[i] = 0
			formatex(UserHatModel[i], 255, "%s", "!NULL")
		}
	}
	client_print(id,print_chat,"[%s] Removed hats from everyone.",PLUG_NAME)
	return PLUGIN_CONTINUE
}

public Set_Hat(player,imodelnum,targeter)
{
	if (imodelnum == 0) {
		if(g_bwEnt[player] > 0) engfunc(EngFunc_RemoveEntity,g_bwEnt[player])
		g_bwEnt[player] = 0
		formatex(UserHatModel[player], 255, "%s", "!NULL")
		PrintChatColor(targeter, _, "!g[%s] !yВы сняли шапку", PLUG_NAME)
	} else {
		if(g_bwEnt[player] < 1) {
			g_bwEnt[player] = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "info_target"))
			if(g_bwEnt[player] > 0) 
			{
				UserHaveHatWith[player] = 'n'
				new mdlName[256]
				new CsTeams:team = cs_get_user_team(player)
				
				formatex(UserHatModel[player], 255, "%s", HATMDL[imodelnum])
				
				set_pev(g_bwEnt[player], pev_movetype, MOVETYPE_FOLLOW)
				set_pev(g_bwEnt[player], pev_aiment, player)
				set_pev(g_bwEnt[player], pev_rendermode, kRenderNormal)
				set_pev(g_bwEnt[player], pev_renderamt, 0.0)
				
				if(HATNAME[imodelnum][0] == 'm')
				{
					UserHaveHatWith[player] = 'm'
					
					if(team == CS_TEAM_CT)
						formatex(mdlName, charsmax(mdlName), "%s/c_%s", HATS_PATH, HATMDL[imodelnum])
					else if(team == CS_TEAM_T)
						formatex(mdlName, charsmax(mdlName), "%s/t_%s", HATS_PATH, HATMDL[imodelnum])
					
					engfunc(EngFunc_SetModel, g_bwEnt[player], mdlName)
				}
				else
				{
					format(mdlName, charsmax(mdlName), "%s/%s", HATS_PATH, HATMDL[imodelnum])
					
					engfunc(EngFunc_SetModel, g_bwEnt[player], mdlName)
				}
				
				if(HATNAME[imodelnum][0] == 's')
				{
					UserHaveHatWith[player] = 's'
					
					if(team == CS_TEAM_CT)
						set_pev(g_bwEnt[player], pev_skin, 1)
					else if(team == CS_TEAM_T)
						set_pev(g_bwEnt[player], pev_skin, 0)
				}
			}
		} else {
				UserHaveHatWith[player] = 'n'
				new mdlName[256]
				new CsTeams:team = cs_get_user_team(player)
				formatex(UserHatModel[player], 255, "%s", HATMDL[imodelnum])
				
				if(HATNAME[imodelnum][0] == 'm')
				{
					UserHaveHatWith[player] = 'm'
					if(team == CS_TEAM_CT)
						formatex(mdlName, charsmax(mdlName), "%s/c_%s", HATS_PATH, HATMDL[imodelnum])
					else if(team == CS_TEAM_T)
						formatex(mdlName, charsmax(mdlName), "%s/t_%s", HATS_PATH, HATMDL[imodelnum])
					
					engfunc(EngFunc_SetModel, g_bwEnt[player], mdlName)
				}
				else
				{
					formatex(mdlName, charsmax(mdlName), "%s/%s", HATS_PATH, HATMDL[imodelnum])
					
					engfunc(EngFunc_SetModel, g_bwEnt[player], mdlName)
				}
				
				if(HATNAME[imodelnum][0] == 's')
				{
					UserHaveHatWith[player] = 's'
					
					if(team == CS_TEAM_CT)
						set_pev(g_bwEnt[player], pev_skin, 1)
					else if(team == CS_TEAM_T)
						set_pev(g_bwEnt[player], pev_skin, 0)
				}
		}
		if(HATNAME[imodelnum][0] == 's' || HATNAME[imodelnum][0] == 'm')
			PrintChatColor(targeter, _, "!g[%s] !yВы одели шапку !g%s", PLUG_NAME, HATNAME[imodelnum][1])
		else
			PrintChatColor(targeter, _, "!g[%s] !yВы одели шапку !g%s", PLUG_NAME, HATNAME[imodelnum])
	}
	
	client_cmd(player, "setinfo ^"next21_hat^" ^"%s^"", UserHatModel[player])
}

public command_load()
{
	if(file_exists(HatFile)) {
		HATMDL[0] = ""
		HATNAME[0] = "Снять шапку"
		TotalHats = 1
		new sfLineData[128]
		new file = fopen(HatFile,"rt")
		while(file && !feof(file)) {
			fgets(file,sfLineData,127)
			
			// Skip Comment and Empty Lines
			if (containi(sfLineData,";") > -1) continue
			
			// BREAK IT UP!
			parse(sfLineData, HATMDL[TotalHats],40,HATNAME[TotalHats],40)
			
			TotalHats += 1
			if(TotalHats >= MAX_HATS) {
				server_print("[%s] Reached hat limit",PLUG_NAME)
				break
			}
		}
		if(file) fclose(file)
	}
	MenuPages = floatround((TotalHats / 8.0), floatround_ceil)
	server_print("[%s] Loaded %i hats, Generated %i pages)",PLUG_NAME,TotalHats,MenuPages)
}

public _n21_get_hat_model(plugin, num_params)
{
	new id = get_param(1)
	new hatMdl[256]
	formatex(hatMdl, charsmax(hatMdl), "%s", UserHatModel[id])
	set_string(2, hatMdl, charsmax(hatMdl))
}

public _n21_set_hat_model(plugin, num_params)
{
	new id = get_param(1)
	new hatMdl[256]
	get_string(2, hatMdl, charsmax(hatMdl))
	
	formatex(UserHatModel[id], 255, "%s", hatMdl)
	
	if(equal(hatMdl, ""))
		return 0
	
	if(equal(hatMdl, "!NULL"))
		return 1
	
	for(new i = 1; i<=TotalHats; i++)
	{
		if(equal(hatMdl, HATMDL[i]))
		{
			Set_Hat_in_reset_hud(id, id, i, id)
			return 1
		}
	}
	
	return 0
}


public _n21_hide_hat(plugin, num_params)
{
	new id = get_param(1)
	
	if(!pev_valid(g_bwEnt[id]))
		return 0
	
	if(get_param(2)) fm_set_rendering(g_bwEnt[id], kRenderFxGlowShell, 0, 0, 0, kRenderTransAlpha, 0)
	else fm_set_rendering(g_bwEnt[id])
	
	return 1
}

public Set_Hat_in_reset_hud(id, player, imodelnum, targeter)
{
	resethud_id[id] = player
	resethud_mn[id] = imodelnum
	resethud_tg[id] = targeter
}
