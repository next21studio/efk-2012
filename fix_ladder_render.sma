#include <amxmodx>
#include <fakemeta_util>

#define PLUGIN	"Fix for map 35hp_3"
#define AUTHOR	"trofian"
#define VERSION	"1.0.1"

public plugin_init(){
	register_plugin(PLUGIN, VERSION, AUTHOR)
	
	new map[16]
	get_mapname(map, charsmax(map))
	
	if(equal(map, "35hp_3")) {
		set_task(5.0, "fix_render")
	}
}

public fix_render() {
	new ent
	while((ent = fm_find_ent_by_target(ent, "func_wall"))) {
		set_pev(ent, pev_rendermode, kRenderTransAlpha)
		set_pev(ent, pev_renderfx, 255)
		
		server_print("[%s] The func_wall with id %d was fixed", PLUGIN, ent)
	}
}