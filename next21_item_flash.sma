#include <amxmodx>
#include <fun>
#include <cstrike>
#include <fakemeta>
#include <hamsandwich>
#include <next21_knife_core>

#define PLUGIN	"Next21 Items"
#define AUTHOR	"trofian and Psycrow"
#define VERSION	"1.1"

#define get_gun_owner(%1)	get_pdata_cbase(%1, 41, 4)

#define g_vFlash	"models/next21_knife_v2/items/flash_grenade/v_flashbang.mdl"
#define g_pFlash	"models/next21_knife_v2/items/flash_grenade/p_flashbang.mdl"
#define g_wFlash	"models/next21_knife_v2/items/flash_grenade/w_flashbang.mdl"

#define g_wFlashbang	"models/w_flashbang.mdl"

#define MAX_AMMO	100
#define CLASS		"weapon__next21_gren_flash"

public plugin_precache()
{
	precache_model(g_vFlash)
	precache_model(g_pFlash)
	precache_model(g_wFlash)
	precache_model(g_wFlashbang)
	
	precache_generic("sprites/weapon__next21_gren_flash.txt")
}

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	n21_register_hud("weapon_flashbang", CLASS)
	
	kc_register_shop_item("get_flash", "Вы купили слеповую гранату", "Слеповая граната", 2000)// 2000)
	RegisterHam(Ham_Item_Deploy, "weapon_flashbang","CurWeapon", 1)
	RegisterHam(Ham_Item_AddToPlayer, "weapon_flashbang", "hook_flash_add_post", 1)
	register_forward(FM_SetModel, "fw_SetModel")
	set_flash()
}

public hook_flash_add_post(gun)
	n21_hud_change_to(get_gun_owner(gun), "weapon_flashbang", CLASS)

public CurWeapon(gun)
{
	new id = get_gun_owner(gun)
	
	set_pev(id, pev_viewmodel2, g_vFlash)
	set_pev(id, pev_weaponmodel2, g_pFlash)
}

public fw_SetModel(entity, model[])
{
	if (!pev_valid(entity)) 
		return FMRES_IGNORED

	if (!equali(model, g_wFlashbang)) 
		return FMRES_IGNORED
	
	new className[33]
	pev(entity, pev_classname, className, 32)
	
	if (equal(className, "weaponbox") || equal(className, "armoury_entity") || equal(className, "grenade"))
	{
		engfunc(EngFunc_SetModel, entity, g_wFlash)
		return FMRES_SUPERCEDE
	}
	
	return FMRES_IGNORED
}

public get_flash(id)
{
	if(!is_user_alive(id))
		return ITEM_DEAD
	
	if(user_has_weapon(id, CSW_FLASHBANG))
	{
		new bp = cs_get_user_bpammo(id, CSW_FLASHBANG)
		
		if(bp >= MAX_AMMO)
			return ITEM_ALREADY_HAVE
		else
		{
			cs_set_user_bpammo(id, CSW_FLASHBANG, bp+1)
			emit_sound(id, CHAN_ITEM, "items/gunpickup2.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
			return ITEM_CONTINUE
		}
	}
	
	give_item(id, "weapon_flashbang")
	
	return ITEM_CONTINUE
}

stock set_flash() {
	new ent = get_maxplayers();
	while ((ent = engfunc(EngFunc_FindEntityByString, ent, "model", g_wFlashbang)))
		engfunc(EngFunc_SetModel, ent,g_wFlash);
}
