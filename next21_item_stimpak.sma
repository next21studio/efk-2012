#include <amxmodx>
#include <fun>
#include <csx>
#include <hamsandwich>
#include <fakemeta_util>
#include <next21_knife_core>
#include <next21_advanced>


#define is_entity_player(%1)		(1<=%1<=g_maxplayers)

#define get_gun_in_hand(%1)		get_pdata_cbase(%1, 373, 5)
#define get_gun_owner(%1)		get_pdata_cbase(%1, 41, 4)
#define get_gun_csw_by_id(%1)		get_pdata_int(%1, 43, 4)

#define PLUGIN	"Regeneration"
#define AUTHOR	"Psycrow"
#define VERSION	"1.1"

#define TASKID 1654742

#define	KnifeVModel		"models/next21_knife_v2/items/stimpak/v_stimpak.mdl"
#define	g_szSoundStimpak	"weapons/syringe/syringe_inject.wav"

new bool:g_InRegen[33], bool:g_AddRegen[33], g_syncHudMessage, // 4 �����
g_maxplayers

public plugin_precache()
{
	precache_model(KnifeVModel)
	precache_sound(g_szSoundStimpak)
}

public plugin_natives()
	register_native("ka_use_regeneration", "_n21_Regeneration", 0) // 1 - ��

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	g_maxplayers = get_maxplayers()
	g_syncHudMessage = CreateHudSyncObj()
	
	kc_register_shop_item("get_stimpak", "Здоровье будет восстанавливаться если у вас мало HP", "Регенерация (200 hp)", 21000) //21000
}

public client_connect(id)
{
	g_InRegen[id] = false   
	g_AddRegen[id] = false
}

public get_stimpak(id)
{
	if(!is_user_alive(id))
		return ITEM_DEAD
	
	new hand_model[128]
	pev(id, pev_viewmodel2, hand_model, charsmax(hand_model))
	
	if(equal(hand_model, KnifeVModel))
		return ITEM_NOT_AVAILABLE
	
	set_pev(id, pev_viewmodel2, KnifeVModel)
	UTIL_PlayWeaponAnimation(id, 0)
	
	set_pdata_float(id, 83, 2.5, 5)
	set_task(2.5, "get_regen", id)
		
	return ITEM_CONTINUE
}

public get_regen(id)
{
	if(!is_user_alive(id))
		return PLUGIN_CONTINUE
	
	engclient_cmd(id, "weapon_knife")
	ExecuteHamB(Ham_Item_Deploy, get_gun_in_hand(id))
	
	if(!ka_is_flashed(id))
		set_flsh(id, 0, 255, 0, 20)
	
	if(g_InRegen[id])
	{
		g_AddRegen[id] = true
		return PLUGIN_CONTINUE
	}
		
	g_InRegen[id] = true
					
	new args[2]
	args[0] = id
	args[1] = 200
	
	regeneration(args)
	
	return PLUGIN_CONTINUE
}

public regeneration(put_args[])
{
	static id, hp, args[2]
	id = put_args[0]
	hp = put_args[1]

	if(!is_user_alive(id))
	{
		args[0] = id
		args[1] = hp
		set_task(1.0, "regeneration", TASKID+id, args, 2)
		return PLUGIN_CONTINUE
	}
	
	if(g_AddRegen[id])
	{
		g_AddRegen[id] = false
		hp += 200
		client_print(id, print_center, "Вы продлили регенерацию до %d hp", hp)
	}
	
	if(hp <= 0)
	{
		g_InRegen[id] = false
		client_print(id, print_center, "Регенерация завершена")
		return PLUGIN_HANDLED
	}
	
	static userhp
	userhp = get_user_health(id)
	
	if(userhp < 25)
	{
		set_user_health(id, userhp+2)
		hp -= 2
		set_hudmessage(255, 0, 0, 0.01, 0.75, 0, 0.6, 0.6, 0.1, 0.1, 4) // ��������� �������� ��� �����
		ShowSyncHudMsg(id, g_syncHudMessage, "Регенерация %d HP (turbo)", hp)
	}
	else if(userhp >= 25 && userhp < kc_get_user_max_hp(id))
	{
		set_user_health(id, userhp+1)
		hp -= 1
		set_hudmessage(0, 255, 0, 0.01, 0.75, 0, 0.6, 0.6, 0.1, 0.1, 4) // ��������� �������� ��� �����
		ShowSyncHudMsg(id, g_syncHudMessage, "Регенерация %d HP", hp)
	}
	
	static i
	for(i=1; i<=g_maxplayers; i++)
	{
		if(is_user_alive(i) || !is_user_connected(i))
			continue
		
		if(pev(i, pev_iuser2) == id)
		{
			if(userhp < 25)
			{
				set_hudmessage(255, 0, 0, 0.01, 0.75, 0, 0.6, 0.6, 0.1, 0.1, 4) // ��������� �������� ��� �����
				ShowSyncHudMsg(i, g_syncHudMessage, "Регенерация %d HP (turbo)", hp)
			}
			else if(userhp >= 25 && userhp < kc_get_user_max_hp(id))
			{
				set_hudmessage(0, 255, 0, 0.01, 0.75, 0, 0.6, 0.6, 0.1, 0.1, 4) // ��������� �������� ��� �����
				ShowSyncHudMsg(i, g_syncHudMessage, "Регенерация %d HP", hp)
			}
		}
	}
	
	args[0] = id
	args[1] = hp
	set_task(0.5, "regeneration", TASKID+id, args, 2)
	
	return PLUGIN_CONTINUE
}

public set_flsh(id, r, g, b, i)
{
	message_begin(MSG_ONE, get_user_msgid("ScreenFade"), _, id)
	write_short(1<<12)
	write_short(1<<8)
	write_short(1<<4)
	write_byte(r)
	write_byte(g)
	write_byte(b)
	write_byte(i)
	message_end()
}

stock UTIL_PlayWeaponAnimation(const Player, const Sequence)
{
   set_pev(Player, pev_weaponanim, Sequence)
   
   message_begin(MSG_ONE_UNRELIABLE, SVC_WEAPONANIM, .player = Player)
   write_byte(Sequence)
   write_byte(pev(Player, pev_body))
   message_end()
}

public _n21_Regeneration(plugin, num_params)
{
	new id = get_param(1)
	
	if(g_InRegen[id])
	{
		g_AddRegen[id] = true
		return PLUGIN_CONTINUE
	}
		
	g_InRegen[id] = true
					
	new args[2]
	args[0] = id
	args[1] = 200
	
	regeneration(args)
	
	return PLUGIN_CONTINUE
}
