#include <amxmodx>
#include <fun>
#include <cstrike>
#include <fakemeta>
#include <hamsandwich>
#include <next21_knife_core>
#include <hud>

#define PLUGIN	"Next21 Items"
#define AUTHOR	"trofian"
#define VERSION	"1.1"

#define get_gun_owner(%1)	get_pdata_cbase(%1, 41, 4)

#define g_vIpad	"models/next21_knife_v2/items/ipad_grenade/v_he_ipad.mdl"
#define g_pIpad	"models/next21_knife_v2/items/ipad_grenade/p_he_ipad.mdl"
#define g_wIpad	"models/next21_knife_v2/items/ipad_grenade/w_he_ipad.mdl"

#define g_wHe	"models/w_hegrenade.mdl"

#define MAX_AMMO	100
#define CLASS		"weapon__next21_gren_ipad"

public plugin_precache()
{
	precache_model(g_vIpad)
	precache_model(g_pIpad)
	precache_model(g_wIpad)
	precache_model(g_wHe)
	
	precache_generic("sprites/weapon__next21_gren_ipad.txt")
}

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	n21_register_hud("weapon_hegrenade", CLASS)

	kc_register_shop_item("get_he", "Вы купили гранату", "Граната (HE)", 6000)// 7500)
	RegisterHam(Ham_Item_Deploy,"weapon_hegrenade","CurWeapon", 1)
	RegisterHam(Ham_Item_AddToPlayer, "weapon_hegrenade", "hook_he_add_post", 1)
	register_forward(FM_SetModel, "fw_SetModel")
	set_Ipad()
}

public hook_he_add_post(gun)
	n21_hud_change_to(get_gun_owner(gun), "weapon_hegrenade", CLASS)

public CurWeapon(gun)
{
	new id = get_gun_owner(gun)
	
	set_pev(id, pev_viewmodel2, g_vIpad)
	set_pev(id, pev_weaponmodel2, g_pIpad)
}

public fw_SetModel(entity, model[])
{
	if (!pev_valid(entity)) 
		return FMRES_IGNORED

	if (!equali(model, g_wHe)) 
		return FMRES_IGNORED
	
	new className[33]
	pev(entity, pev_classname, className, 32)
	
	if (equal(className, "weaponbox") || equal(className, "armoury_entity") || equal(className, "grenade"))
	{
		engfunc(EngFunc_SetModel, entity, g_wIpad)
		return FMRES_SUPERCEDE
	}
	
	return FMRES_IGNORED
}

public get_he(id)
{
	if(!is_user_alive(id))
		return ITEM_DEAD
	
	if(user_has_weapon(id, CSW_HEGRENADE))
	{
		new bp = cs_get_user_bpammo(id, CSW_HEGRENADE)
		
		if(bp >= MAX_AMMO)
			return ITEM_ALREADY_HAVE
		else
		{
			cs_set_user_bpammo(id, CSW_HEGRENADE, bp+1)
			emit_sound(id, CHAN_ITEM, "items/gunpickup2.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
			return ITEM_CONTINUE
		}
	}
		
	give_item(id, "weapon_hegrenade")
	
	return ITEM_CONTINUE
}

set_Ipad()
{
	new ent = get_maxplayers();
	while ((ent = engfunc(EngFunc_FindEntityByString, ent, "model", g_wHe)))
		engfunc(EngFunc_SetModel, ent, g_wIpad);
}
