#include <amxmodx>
#include <fakemeta_util>
#include <next21_advanced>
#include <hamsandwich>
#include <next21_knife_core>
#include <WPMGPrintChatColor>
#include <hud>

#define PLUGIN	"Candy Cane"
#define AUTHOR	"Psycrow" // and mini adaptation by trofian
#define VERSION	"1.1"

#define IsEntityPlayer(%1)	(1<=%1<=g_maxplayers)
#define get_gun_owner(%1)	get_pdata_cbase(%1, 41, 4)
#define get_gun_in_hand(%1)	get_pdata_cbase(%1, 373, 5)
#define get_gun_csw_by_id(%1)	get_pdata_int(%1, 43, 4)

#define TASKID 131742

#define CLASSNAME		"weapon__next21_candy"
#define KnifeVModel		"models/next21_knife_v2/items/candy_cane/v_candy_cane.mdl"
#define	KnifePModel		"models/next21_knife_v2/items/candy_cane/p_candy_cane.mdl"

#define g_szSoundCanyKnifeHit	"next21_knife_v2/items/candy/knife_hit1.wav"

#define max_round 		3

new bool:g_HaveCandy[33], bool:g_InCandy[33], g_RoundLeft[33], g_maxplayers

public plugin_natives()
	register_native("ka_is_in_candy", "_ka_is_in_candy", 0)

public plugin_precache()
{
	precache_model(KnifeVModel)
	precache_model(KnifePModel)
	
	precache_sound(g_szSoundCanyKnifeHit)
	
	precache_generic("sprites/weapon__next21_candy.txt")
}

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	
	n21_register_hud("weapon_knife", CLASSNAME)
	kc_register_shop_item("get_candy", "Нажите на 'G' чтобы сменить нож на карамельную трость", "Карамельная трость (100% подарок)", 15000) // 15000
	RegisterHam(Ham_Item_Deploy,"weapon_knife","CurWeapon", 1)
	RegisterHam(Ham_Player_PreThink, "player", "Ham_PreThink_player")
	g_maxplayers = get_maxplayers()
	register_event("HLTV", "NewRound", "a", "1=0", "2=0")
	register_clcmd("drop", "change_to_candy")
	register_forward(FM_EmitSound, "EmitSound")
}

public Ham_PreThink_player(id)
{
	if(!is_user_alive(id))
		return
	
	if(g_InCandy[id])
		kc_set_crosshair(id, 0, CrossOff)
}

public kc_ability_pre(id, victim)
{
	if(g_InCandy[id])
		return PLUGIN_HANDLED
	
	return PLUGIN_CONTINUE
}

public client_connect(id)
{
    g_HaveCandy[id] = false
    g_InCandy[id] = false
}

public NewRound() set_task(0.2, "NewRoundDelayed")
public NewRoundDelayed()
{
	for(new i=1; i<=g_maxplayers; i++)
	{	
		if(!is_user_connected(i) || !g_HaveCandy[i])
			continue
		
		if(g_InCandy[i] && is_user_alive(i))
			kc_set_crosshair(i, 0, CrossOff)
		
		if(g_RoundLeft[i] == max_round-1)
			PrintChatColor(i, _, "!g[%s] !yЭто последний раунд с карамельной тростью", PLUGIN)
		else if(g_RoundLeft[i] == max_round)
		{
			g_HaveCandy[i] = false
			g_InCandy[i] = false
			
			if(is_user_alive(i))
			{
				engclient_cmd(i, "weapon_knife")
				ExecuteHamB(Ham_Item_Deploy, get_gun_in_hand(i))
				kc_set_crosshair(i, 0)
			}
			
			g_RoundLeft[i] = 0
			continue
		}
		
		g_RoundLeft[i]++
	}
}

public change_to_candy(id)
{
	if(!is_user_alive(id))
		return PLUGIN_CONTINUE
	
	if(!g_HaveCandy[id] || get_gun_csw_by_id(get_gun_in_hand(id)) != CSW_KNIFE)
		return PLUGIN_CONTINUE
		
	if(ka_in_ninja(id))
		return PLUGIN_CONTINUE
	
	if(g_InCandy[id])
	{
		g_InCandy[id] = false
		kc_set_crosshair(id, 0)
	}
	else
	{
		g_InCandy[id] = true
		kc_set_crosshair(id, 0, CrossOff)
	}
	
	engclient_cmd(id, "weapon_knife")
	ExecuteHamB(Ham_Item_Deploy, get_gun_in_hand(id))
	UTIL_PlayWeaponAnimation(id, 3)
	
	return PLUGIN_HANDLED
}

public get_candy(id)
{
	if(!is_user_alive(id))
		return ITEM_DEAD
	
	if(g_HaveCandy[id])
		return ITEM_ALREADY_HAVE
	
	g_HaveCandy[id] = true
	
	return ITEM_CONTINUE
}

public CurWeapon(weapon)
{
	new id = get_gun_owner(weapon)
	
	if(g_InCandy[id])
	{
		set_pev(id, pev_viewmodel2, KnifeVModel)
		set_pev(id, pev_weaponmodel2, KnifePModel)
		
		n21_hud_change_to(id, "weapon_knife", CLASSNAME)
	}
}

public EmitSound(id, channel, const sample[], Float:volume, Float:attn, flags, pitch)
{
	if(IsEntityPlayer(id)){
	
	if(g_InCandy[id])
	{
		if(equal(sample, "weapons/knife_hit1.wav"))
		{
			emit_sound(id, channel, g_szSoundCanyKnifeHit, volume, attn, flags, pitch)
			return FMRES_SUPERCEDE
		}
		if(equal(sample, "weapons/knife_hit2.wav"))
		{
			emit_sound(id, channel, g_szSoundCanyKnifeHit, volume, attn, flags, pitch)
			return FMRES_SUPERCEDE
		}
		if(equal(sample, "weapons/knife_hit3.wav"))
		{
			emit_sound(id, channel, g_szSoundCanyKnifeHit, volume, attn, flags, pitch)
			return FMRES_SUPERCEDE
		}
		if(equal(sample, "weapons/knife_hit4.wav"))
		{
			emit_sound(id, channel, g_szSoundCanyKnifeHit, volume, attn, flags, pitch)
			return FMRES_SUPERCEDE
		}
		if(equal(sample, "weapons/knife_hitwall1.wav"))
		{
			emit_sound(id, channel, g_szSoundCanyKnifeHit, volume, attn, flags, pitch)
			return FMRES_SUPERCEDE
		}
		if(equal(sample, "weapons/knife_hitwall2.wav"))
		{
			emit_sound(id, channel, g_szSoundCanyKnifeHit, volume, attn, flags, pitch)
			return FMRES_SUPERCEDE
		}
		if(equal(sample, "weapons/knife_stab.wav"))
		{
			emit_sound(id, channel, g_szSoundCanyKnifeHit, volume, attn, flags, pitch)
			return FMRES_SUPERCEDE
		}
	}
	}
	return FMRES_IGNORED
}

public bool:_ka_is_in_candy(plugin, num_params)
{
	if(!IsEntityPlayer(get_param(1)))
		return false
	
	return g_InCandy[get_param(1)]
}

stock UTIL_PlayWeaponAnimation(const Player, const Sequence)
{
   set_pev(Player, pev_weaponanim, Sequence)
   
   message_begin(MSG_ONE_UNRELIABLE, SVC_WEAPONANIM, .player = Player)
   write_byte(Sequence)
   write_byte(pev(Player, pev_body))
   message_end()
}
/* AMXX-Studio Notes - DO NOT MODIFY BELOW HERE
*{\\ rtf1\\ ansi\\ deff0{\\ fonttbl{\\ f0\\ fnil Tahoma;}}\n\\ viewkind4\\ uc1\\ pard\\ lang1049\\ f0\\ fs16 \n\\ par }
*/
