#include <amxmodx>
#include <hamsandwich>
#include <cstrike>
#include <fun>
#include <next21_knife_core>
#include <next21_advanced>

#define PLUGIN	"Next21 VIP"
#define AUTHOR	"trofian"
#define VERSION	"1.1"

#define VIP_FLAG	ADMIN_LEVEL_H // ���� t
#define MAXMONEY 55000

new g_msgScoreAttrib

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	RegisterHam(Ham_Spawn, "player", "hook_spawn_post", 1)
	g_msgScoreAttrib = get_user_msgid("ScoreAttrib")
}

public hook_spawn_post(id)
{
	if(!is_user_alive(id))
		return
	
	if(!(get_user_flags(id) & VIP_FLAG))
		return
	
	message_begin(MSG_ALL, g_msgScoreAttrib)
	write_byte(id)
	write_byte(4)
	message_end()
	
	new player_money = cs_get_user_money(id)
	new add_money = random_num(2500, 4500)
	
	if(player_money+add_money > MAXMONEY)
		add_money = 0
	
	cs_set_user_money(id, player_money+add_money)
	
	give_item(id, "weapon_hegrenade")
	give_item(id, "weapon_flashbang")
	ka_frostgren_give(id)
	ka_gazgren_give(id)
	engclient_cmd(id, "weapon_knife")
}
