#include <amxmodx>
#include <fun>
#include <cstrike>
#include <fakemeta>
#include <hamsandwich>
#include <fakemeta_util>
#include <next21_knife_core>
#include <next21_advanced>

#define PLUGIN	"Next21 HP Item"
#define AUTHOR	"Psycrow"
#define VERSION	"1.0"

#define get_gun_in_hand(%1)	get_pdata_cbase(%1, 373, 5)

#define	KnifeVModel		"models/next21_knife_v2/items/stimpak/v_stimpak.mdl"
#define	g_szSoundStimpak	"weapons/syringe/syringe_inject.wav"

public plugin_precache()
{
	precache_model(KnifeVModel)
	precache_sound(g_szSoundStimpak)
}

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	kc_register_shop_item("get_stimpak", "Вы купили 50 HP", "50 HP", 15000)// 15000)
}

public get_stimpak(id)
{
	if(!is_user_alive(id))
		return ITEM_DEAD
	
	if(get_user_health(id) > 150)
		return ITEM_ALREADY_HAVE
				
	new hand_model[128]
	pev(id, pev_viewmodel2, hand_model, charsmax(hand_model))
	
	if(equal(hand_model, KnifeVModel))
		return ITEM_NOT_AVAILABLE
	
	set_pev(id, pev_viewmodel2, KnifeVModel)
	UTIL_PlayWeaponAnimation(id, 0)
	
	set_pdata_float(id, 83, 2.5, 5);
	set_task(2.5, "get_hp", id)
		
	return ITEM_CONTINUE
}

public get_hp(id)
{	
	if(!is_user_alive(id))
		return ITEM_DEAD

	engclient_cmd(id, "weapon_knife")
	ExecuteHamB(Ham_Item_Deploy, get_gun_in_hand(id))
	
	if(!ka_is_flashed(id))
		set_flsh(id, 0, 255, 0, 25)
	
	fm_set_user_health(id, floatround(pev(id, pev_health)+50.0))
	
	return ITEM_CONTINUE
}

public set_flsh(id, r, g, b, i)
{
	message_begin(MSG_ONE, get_user_msgid("ScreenFade"), _, id)
	write_short(1<<12)
	write_short(1<<8)
	write_short(1<<4)
	write_byte(r)
	write_byte(g)
	write_byte(b)
	write_byte(i)
	message_end()
}

stock UTIL_PlayWeaponAnimation(const Player, const Sequence)
{
   set_pev(Player, pev_weaponanim, Sequence)
   
   message_begin(MSG_ONE_UNRELIABLE, SVC_WEAPONANIM, .player = Player)
   write_byte(Sequence)
   write_byte(pev(Player, pev_body))
   message_end()
}
