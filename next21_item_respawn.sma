#include <amxmodx>
#include <hamsandwich>
#include <cstrike>
#include <next21_knife_core>

#define PLUGIN	"Next21 Respawn"
#define AUTHOR	"trofian"
#define VERSION	"1.0"

new bool:g_was_respawned[33]

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	register_event("HLTV", "NewRound", "a", "1=0", "2=0")
	kc_register_shop_item("respawn", "Вы возродились", "Возродиться", 15000) // 15000
}

public respawn(id)
{
	if(is_user_alive(id))
		return ITEM_ALIVE
	
	new CsTeams:team = cs_get_user_team(id)
	
	if(g_was_respawned[id])
		return ITEM_NOT_AVAILABLE
	
	if(team != CS_TEAM_SPECTATOR)
		ExecuteHam(Ham_CS_RoundRespawn, id)
	else
		return ITEM_NOT_AVAILABLE
	
	g_was_respawned[id] = true
	
	return ITEM_CONTINUE
}

public NewRound()
	for(new i=1; i<=32; i++)
		g_was_respawned[i] = false
